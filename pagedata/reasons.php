<?php
    $aColumns     = array(
        'reasonid',
        'description',
        'reasonid as action'
    );
    $sIndexColumn = 'reasonid';
    $sTable       = 'reasons';

    $join         = array();
    $additionalSelect = array();

    $where = array();
    $filter = array();

    // filter something
    if (!empty($_POST['something'])) {
        array_push($filter, 'AND (1 = 1)');
    }

    // where with filter
    if (count($filter) > 0) {
        array_push($where, 'AND ('.filterDataTable($filter).')');
    }

    $result           = createDataTable($db, $aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);
    $output           = $result['output'];
    $rResult          = $result['rResult'];

    foreach ($rResult as $aRow) {
        $row = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if (strpos($aColumns[$i],'as') !== false && !isset($aRow[ $aColumns[$i] ])){
                $_data = $aRow[ string_after($aColumns[$i], 'as ')];
            } else {
                $_data = $aRow[ $aColumns[$i] ];
            }

            if (stripos($aColumns[$i], 'action') !== false) {
                $_data = '<div class="text-center">';
                $_data .= '<a class="text-center" href="?p=reason-edit&id=' . $aRow['reasonid'] . '&url=' . base64_encode($_SERVER['HTTP_REFERER']) . '"><button><i class="fa fa-edit"></i> Edit</button></a>';
                $_data .= '&nbsp;';
                $_data .= '<a class="text-center" href="?p=reason-delete&id=' . $aRow['reasonid'] . '&url=' . base64_encode($_SERVER['HTTP_REFERER']) . '"><button><i class="fa fa-edit"></i> DELETE</button></a>';
                $_data .= '</div>';
            }

            $row[] = $_data;
        }

        $output['aaData'][] = $row;
    }

    header('Content-Type: application/json');
    echo json_encode($output);
