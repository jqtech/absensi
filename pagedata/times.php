<?php
    $aColumns     = array(
        'whenin as tanggal',
        'username',
        'reason',
        'IFNULL(unitin.location, locationin) as locationin',
        'whenin',
        'whenout',
        'timeid as action'
    );
    $sIndexColumn = 'timeid';
    $sTable       = 'timeclock';

   $join         = array(
        'LEFT JOIN units as unitin ON unitin.locationid = timeclock.locationin',
        'LEFT JOIN units as unitout ON unitout.locationid = timeclock.locationout',
        'LEFT JOIN users ON users.id = timeclock.userid'
    );
    $additionalSelect = array(
        'timeid',
        'userid',
        'unitin',
        'unitout',
        'IFNULL(unitout.location, IFNULL(locationout, "UNKNOWN")) as locationout',
        'name'
    );

    $where = array();
    $filter = array();

     // filter date
    if (!empty($_POST['pstart'])) {
        array_push($filter, 'AND (DATE(whenin) >= "' . date('Y-m-d', strtotime($_POST['pstart'])) . '")');
    }
    if (!empty($_POST['puntil'])) {
        array_push($filter, 'AND (DATE(whenin) <= "' . date('Y-m-d', strtotime($_POST['puntil'])) . '")');
    }

    // where with filter
    if (count($filter) > 0) {
        array_push($where, 'AND ('.filterDataTable($filter).')');
    }

    $result           = createDataTable($db, $aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);
    $output           = $result['output'];
    $rResult          = $result['rResult'];

    foreach ($rResult as $aRow) {
        $row = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if (strpos($aColumns[$i],'as') !== false && !isset($aRow[ $aColumns[$i] ])){
                $_data = $aRow[ string_after($aColumns[$i], 'as ')];
            } else {
                $_data = $aRow[ $aColumns[$i] ];
            }

            if (stripos($aColumns[$i], 'tanggal') !== false) {
                $_data = date('Y-m-d', strtotime($_data));
            }
            if ($aColumns[$i] == 'username') {
                $_data = str_pad($aRow['userid'], 12, '0', STR_PAD_LEFT) . ' &ndash; ' . $aRow['username'];
                if (strtoupper($aRow['userid']) == 'GUEST' && is_numeric($aRow['username'])) {
                    if ($guest = $db->row("SELECT * FROM users WHERE id=:name", array('name' => $aRow['username']))) {
                        $_data = str_pad($guest['id'], 12, '0', STR_PAD_LEFT) . ' &ndash; ' . $guest['name'];
                    } else {
                        $_data .= ' <a href="?p=user-edit&id=' . $aRow['username'] . '&url=' . base64_encode('//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) . '" title="User ID Not Found. Save?"><i class="fa fa-save noprint"></i></a>';
                    }
                }
            }
            if ($aColumns[$i] == 'whenin' || $aColumns[$i] == 'whenout') {
                $_data = empty($_data)? '' : date('H:i', strtotime($_data));

                if ($aColumns[$i] == 'whenin') {
                    $location = empty($aRow['unitin'])? '' : ' <span class="hidden-xs a-pointer" style="color:grey;"" title="' . $aRow['locationin'] . ' &ndash; Unit ' . (is_numeric($aRow['unitin'])? 'No.' : '') . $aRow['unitin'] . '">[' . $aRow['unitin'] . ']</span>';
                } else {
                    if (date('Ymd', strtotime($aRow['whenin'])) != date('Ymd', strtotime($aRow['whenout']))) {
                        $_data = '<span class="a-pointer" style="color:orange;" title="' . date('Y-m-d', strtotime($aRow['whenout'])) . '">' . $_data . '</span>';
                    }
                    $location = empty($aRow['unitout'])? '' : ' <span class="hidden-xs a-pointer" style="color:grey;"" title="' . $aRow['locationout'] . ' &ndash; Unit ' . (is_numeric($aRow['unitin'])? 'No.' : '') . $aRow['unitout'] . '">[' . $aRow['unitout'] . ']</span>';
                    if ($aRow['locationin'] != $aRow['locationout']) {
                        $location = str_ireplace('grey', 'orange', $location);
                    }
                }
                $_data .= $location;
            }            
            if (stripos($aColumns[$i], 'action') !== false) {
                $_data = '<a class="center-block text-center" href="?p=user-edit&id=' . $aRow['timeid'] . '&url=' . base64_encode($_SERVER['HTTP_REFERER']) . '"><button><i class="fa fa-edit"></i> Edit</button></a>';
            }

            $row[] = $_data;
        }

        $output['aaData'][] = $row;
    }

    header('Content-Type: application/json');
    echo json_encode($output);
