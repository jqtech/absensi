<?php
    $aColumns     = array(
        'id',
        'name',
        'email',
        'phone',
        'address',
        'company',
        'department',
        'position',
        'status',
        'id as action'
    );
    $sIndexColumn = 'id';
    $sTable       = 'users';

    $join         = array();
    $additionalSelect = array(
        'email2',
        'phone2',
        'locationid'
    );

    $where = array();
    $filter = array();

    // filter something
    if (!empty($_POST['something'])) {
        array_push($filter, 'AND (1 = 1)');
    }

    // where with filter
    if (count($filter) > 0) {
        array_push($where, 'AND ('.filterDataTable($filter).')');
    }

    $result           = createDataTable($db, $aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);
    $output           = $result['output'];
    $rResult          = $result['rResult'];

    foreach ($rResult as $aRow) {
        $row = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if (strpos($aColumns[$i],'as') !== false && !isset($aRow[ $aColumns[$i] ])){
                $_data = $aRow[ string_after($aColumns[$i], 'as ')];
            } else {
                $_data = $aRow[ $aColumns[$i] ];
            }

            if ($aColumns[$i] == $sIndexColumn) {
                $_data = str_pad($_data, 12, '0', STR_PAD_LEFT);
                $icon = '';
                //$icon = strtolower($aRow['status']) != 'waiting payment'? '': 'bookmark-o';
                $_data = '<span class="center-block text-center text-nowrap"><i class="fa fa-' . $icon . '"></i> ' . $_data . '</span>';
            }
            if ($aColumns[$i] == 'email') {
                $mails = array($_data);
                if (!empty($aRow['email2'])) {
                    $mails[] = $aRow['email2'];
                }
                $_data = '';
                foreach ($mails as $mail) {
                    $_data .= '<a style="border:1px solid grey; color:#333; margin-top:5px;" class="label inline-block pull-left" href="mailto:' . $mail . '" target="_blank">' . $mail . '</span>';
                }

            }
            if ($aColumns[$i] == 'phone') {
                $phones = array($_data);
                if (!empty($aRow['phone2'])) {
                    $phones[] = $aRow['phone2'];
                }
                $_data = '';
                foreach ($phones as $phone) {
                    $_data .= '<a style="border:1px solid grey; color:#333; margin-top:5px;" class="label inline-block pull-left" href="https://wa.me/' . preg_replace('/^0?/', '62', $phone) . '" target="_blank">' . $phone . '</span>';
                }
            }
            if (stripos($aColumns[$i], 'action') !== false) {
                $_data = '<a class="center-block text-center" href="?p=user-edit&id=' . $aRow['id'] . '&url=' . base64_encode($_SERVER['HTTP_REFERER']) . '"><button><i class="fa fa-edit"></i> Edit</button></a>';
            }

            $row[] = $_data;
        }

        $output['aaData'][] = $row;
    }

    header('Content-Type: application/json');
    echo json_encode($output);
