<h3 class="well">Data Absensi Mingguan</h3>
</a>

<div id="container">
	<div class="col-md-12">
		<?php
			if (empty($period)) {
				$period = 'week';
			}
            if (empty($_GET['date'])) {
            	$period = 'week';
                if ($period == 'day') $_GET['date'] = time();
                if ($period == 'week') $_GET['date'] = strtotime('monday this week');
                if ($period == 'month') $_GET['date'] = strtotime('first monday of this month');
                if ($period == 'year') $_GET['date'] = strtotime('01-01-'.date('Y'));
                $_GET['date'] = date('Ymd', $_GET['date']);
            }
            $ymd_start = DateTime::createFromFormat('Ymd', $_GET['date'])->format('d-m-Y');
            $time_start = strtotime($ymd_start);

            if (empty($_GET['end'])) {
                $_GET['end'] = strtotime('+1 '.$period.' -1 day',  $time_start);
                $_GET['end'] = date('Ymd', $_GET['end']);
            }
            $ymd_end = DateTime::createFromFormat('Ymd', $_GET['end'])->format('d-m-Y');
            $time_end = strtotime($ymd_end);

            $prev_start = date('Ymd', strtotime('-1 '.$period, $time_start));
            $prev_end = date('Ymd', strtotime('-1 day',  $time_start));
            $next_start = date('Ymd', strtotime('+1 day',  $time_end));
            $next_end = date('Ymd', strtotime('+1 '.$period, $time_end));

            $prev_range = 'date='.$prev_start;
            $next_range = 'date='.$next_start;
            if ($period!='day') {
                $prev_range = 'date='.$prev_start.'&end='.$prev_end;
                $next_range = 'date='.$next_start.'&end='.$next_end;
                $date_range = 'date='.date('Ymd', $time_start).'&end='.date('Ymd', $time_end);
            } else {
                $date_range = 'date='.date('Ymd', strtotime('monday this week', $time_start));
            }
        ?>
        <div class="pull-right">
            <label class="control-label" style="padding-right:10px;">
                Harian
            </label>
            <label for="cgroup" class="control-label">
                <a href="?p=time-sheet&<?= $date_range; ?>" data-toggle="tooltip" title="Ubah ke tampilan Harian"> <i class="fa fa-exchange"></i> </a>
            </label>
        </div>
        <div class="pull-right" style="clear:both;">
            <label for="cgroup" class="control-labe">
                <a href="?p=time-week&<?= $prev_range; ?>" data-toggle="tooltip" title="View previous <?= $period; ?>"> <i class="fa fa-chevron-left"></i> </a>
            </label>
            <label class="control-label" style="font-weight:normal; font-size:30px; padding:0 15px 0; cursor:pointer;" onclick="$('#dtpicker1').trigger('focus');">
                <?= ($period=='day'? '': date('d'. (date('m', $time_start)!=date('m', $time_end)? ' M': ''), $time_start).' - ').date('d M', $time_end); ?>
                <input id="dtpicker" type="text" value="<?= date('Y-m-d', $time_start); ?>" style="width:0; height:0; border:0;" />
            </label>
            <label for="cgroup" class="control-label">
                <a href="?p=time-week&<?= $next_range; ?>" data-toggle="tooltip" title="View next <?= $period; ?>"> <i class="fa fa-chevron-right"></i> </a>
            </label>
        </div>

		<table class="table table-striped table-bordered table-condensed" width="100%">
			<thead>
				<tr>
					<th rowspan="2" width="10%" class="hidden hide">Tanggal</th>
					<th rowspan="2" width="30%">Pegawai</th>
					<?php for($i = $time_start; $i <= $time_end; $i = $i + 86400): ?>
    				<th colspan="2"><?= date('d M', $i); ?></th>
    				<?php endfor; ?>
				</tr>
				<tr>
					<?php for($i = $time_start; $i <= $time_end; $i = $i + 86400): ?>
    				<th>IN</th>
    				<th>OUT</th>
    				<?php endfor; ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($db->query("SELECT DISTINCT IF(id IS NULL, userid, id) userid, username FROM timeclock t LEFT JOIN users u ON t.username = u.name WHERE DATE(whenin) BETWEEN :start AND :until ORDER BY whenin, username", array('start'=>date('Y-m-d', $time_start), 'until'=>date('Y-m-d', $time_end))) as $aRow1): ?>
				<tr>
					<td class="text-center hidden">
						<?= @$aRow1['whenin']; ?>
					</td>
					<td>
						<?php
							$_data = (!is_numeric($aRow1['userid'])? $aRow1['userid']: str_pad($aRow1['userid'], 12, '0', STR_PAD_LEFT)) . ' &ndash; ' . $aRow1['username'];
							if (strtoupper($aRow1['userid']) == 'GUEST' && is_numeric($aRow1['username'])) {
			                    if ($guest = $db->row("SELECT * FROM users WHERE id=:name", array('name' => $aRow1['username']))) {
			                        $_data = str_pad($guest['id'], 12, '0', STR_PAD_LEFT) . ' &ndash; ' . $guest['name'];
			                    } else {
			                        $_data .= ' <a href="?p=user-edit&id=' . $aRow1['username'] . '&url=' . base64_encode('//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) . '" title="User ID Not Found. Save?"><i class="fa fa-save noprint"></i></a>';
			                    }
			                }
						?>
						<?= $_data; ?>
					</td>
					<?php for($i = $time_start; $i <= $time_end; $i = $i + 86400): ?>
						<?php
							$_data_in = array();
							$_data_out = array();
						?>
						<?php foreach ($db->query("SELECT timeclock.*, IFNULL(unitin.location, locationin) as locationin, IFNULL(unitout.location, locationin) as locationout FROM timeclock LEFT JOIN units as unitin ON unitin.locationid = timeclock.locationin LEFT JOIN units as unitout ON unitout.locationid = timeclock.locationout WHERE DATE(whenin)=:tgl AND username=:name ORDER BY whenin", array('tgl'=>date('Y-m-d', $i), 'name'=>$aRow1['username'])) as $aRow): ?>
    						<?php
    							$_data_in[] = date('H:i', strtotime($aRow['whenin']));
    							$_data_in[count($_data_in) - 1] .= empty($aRow['unitin'])? '' : ' <span class="hidden-xs a-pointer" style="color:grey;"" title="' . $aRow['locationin'] . ' &ndash; Unit ' . (is_numeric($aRow['unitin'])? 'No.' : '') . $aRow['unitin'] . '">[' . $aRow['unitin'] . ']</span>';

    							$_data_out[] = date('H:i', strtotime($aRow['whenout']));
    							if (empty($aRow['whenout'])) {
    								$_data_out[count($_data_out) - 1] = '';
    							} else {
	    							if (date('Ymd', strtotime($aRow['whenin'])) != date('Ymd', strtotime($aRow['whenout']))) {
				                        $_data_out[count($_data_out) - 1] = '<span class="a-pointer" style="color:orange;" title="' . date('Y-m-d', strtotime($aRow['whenout'])) . '">' . $_data_out[count($_data_out) - 1] . '</span>';
				                    }
				                    $location = empty($aRow['unitout'])? '' : ' <span class="hidden-xs a-pointer" style="color:grey;"" title="' . $aRow['locationout'] . ' &ndash; Unit ' . (is_numeric($aRow['unitin'])? 'No.' : '') . $aRow['unitout'] . '">[' . $aRow['unitout'] . ']</span>';
				                    if ($aRow['locationin'] != $aRow['locationout']) {
				                        $location = str_ireplace('grey', 'orange', $location);
				                    }
			                    	$_data_out[count($_data_out) - 1] .= $location;
    							}
    						?>
						<?php endforeach; ?>
						<td class="text-center text-nowrap">
							<div style="color:blue;"><?= implode(PHP_EOL.'<br/>', $_data_in); ?></div>
						</td>
						<td class="text-center text-nowrap">
							<div style="color:green;"><?= implode(PHP_EOL.'<br/>', $_data_out); ?></div>
						</td>
    				</td>
    				<?php endfor; ?>
				</tr>
				<?php endforeach; ?>
			</tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
	var set_datepicker = function(obj) {
		var setdate = $(obj).datepicker('getDate');
		if (typeof setdate.setDate != 'undefined') setdate.setDate(setdate.getDate());
		$('#until').datepicker('option','minDate',setdate); 
	}
	jQuery(document).ready(function($) {
		$('#dtpicker').datepicker({
			changeMonth: false,
			changeYear: true, 
            showOtherMonths: true,
            selectOtherMonths: true,
            firstDay: 1,
			minDate: new Date(2021, 1 - 1, 1),
			dateFormat: "yy-mm-dd",
        }).change(function() {
            document.location.href = '?p=time-week&date=' + $("#dtpicker").val().replace(/-/g, '');
        });

		$('#from, #until').on('change', function(){
			$('.table').DataTable().ajax.reload();
		}).datepicker({
			changeMonth: false,
			changeYear: true, 
            showOtherMonths: true,
            selectOtherMonths: true,
            firstDay: 1,
			minDate: new Date(2021, 1 - 1, 1),
			dateFormat: "yy-mm-dd",
			onSelect: function(dateStr) {
				if ($(this).attr('id')!='until') set_datepicker(this);
				$(this).trigger('change');
			}
		}).attr('autocomplete', 'off');

		var searchparam = {};
	    $('.datatable-filters').each(function(){
	        searchparam[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
	    });
		initDataTable('.table', 'undefined', 'undefined', 'undefined', searchparam, [[1, 'ASC']]);
	});
</script>
<style type="text/css">
	.table th {
		vertical-align: top!important;
	}
	.table th:not(:nth-child(1)) {
		text-align: center;
	}
</style>
