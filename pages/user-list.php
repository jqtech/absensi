<h3 class="well">Data Pegawai</h3>

<a href="?p=user-edit&url=<?=base64_encode('//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>" class="btn btn-success pull-right" style="height:34px; margin:-70px 15px;">
	<i class="fa fa-plus visible-xs"></i> <span class="hidden-xs">Add Data</span>
</a>

<div id="container">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-center">ID</th>
					<th class="text-left">Nama</th>
					<th class="text-left">Email</th>
					<th class="text-left">Telpon</th>
					<th class="text-left hidden hide">Alamat</th>
					<th class="text-left">Perusahaan</th>
					<th class="text-left">Departemen</th>
					<th class="text-left">Jabatan</th>
					<th class="text-left hidden hide">Status</th>
					<th class="text-center" style="width:50px">&nbsp;</th>
				</tr>
			</thead>
        </table>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		initDataTable('.table', $('.navbar-brand').attr('href') + '?d=users', 'undefined', 'undefined', [], [0, 'ASC']);
	});
</script>
<style type="text/css">
	.table [tabindex="0"] {
		text-align: center;
	}
</style>
