<?php
    $msg = '';
    $GLOBALS['redirect'] = !empty($_REQUEST['url'])? base64_decode($_REQUEST['url']) : '?p=unit-list';

    if (isset($_POST['id'])) { 
        $id          = $_POST['id'];
        $locationid  = $_POST['locationid'];
        $location    = $_POST['location'];
        $description = $_POST['description'];
        
        // check to make sure important fields are entered
         if (trim($locationid) == '' || trim($location) == '') {
            $msg = 'Please fill all the required fields!';
        }
        if ($msg == '') {
            $check = $db->row("SELECT * FROM units WHERE locationid != '{$id}' AND locationid = '{$locationid}'");
            if ($check) {
                $msg = 'Location ID already in use!';
            }
        }
        if ($msg == '') {
            $param = array(
                'locationid' => $locationid,
                'location' => $location,
                'description' => $description
            );
            if (!empty($id)) {
                $db->query("UPDATE units SET locationid=:locationid, location=:location, description=:description WHERE locationid = '{$id}';", $param); 
            } else {
                $db->query("INSERT units SET locationid=:locationid, location=:location, description=:description;", $param);
            }

            if (trim($msg) == '')  {
                $msg = 'Data sucessfully saved!';
                echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$GLOBALS['redirect'].'">'; 
            }
        }
    } else {
        $id = isset($_GET['id'])? $_GET['id'] : '';

        // if the form hasn't been submitted, display the form
        $result = $db->row("SELECT * FROM units WHERE locationid = '{$id}';");

        $locationid  = $result? $result['locationid'] : $id;
        $location    = $result? $result['location'] : '';
        $description = $result? $result['description'] : '';

        if (!$result) {
            $id = '';
        }
    }
?>

<h3 class="well">
    <?=!empty($id)? 'Edit' : 'Tambah'; ?> Data Lokasi Absensi
</h3>
<div class="alert alert-<?= stripos($msg, 'sucessfully') !== false? 'success' : 'danger'; ?> col-md-12 <?= $msg == ''? 'hidden' : ''; ?>">
    <b class="<?= stripos($msg, 'sucessfully') !== false? 'hidden' : ''; ?>">ERROR:</b> <?= $msg; ?>
</div>

<div id="container">
    <form method="post" class="col-md-12">
        <input type="hidden" name="id" value="<?=$id;?>" />
        <table border="0" class="col-md-6">
            <tr>
                <td class="required" width="135px">ID:</td>
                <td align="right">
                    <input type="text" name="locationid" value="<?=$locationid;?>" />
                </td>
            </tr>
            <tr>
                <td class="required">Lokasi:</td>
                <td align="right">
                    <input type="text" name="location" value="<?=$location;?>" />
                </td>
            </tr>
            <tr>
                <td>Deskripsi: </td>
                <td align="right">
                    <textarea id="description" name="description"><?=$description?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <input type="button" class="btn btn-warning pull-left hidden" value="CANCEL" style="width:auto; margin-right:10px;" onclick="document.location.href='<?=$GLOBALS['redirect'];?>';" />
                  <? if (!empty($id)): ?>
                    <input type="hidden" name="url" value="<?=$_REQUEST['url'];?>" />
                    <input type="button" class="btn btn-danger pull-left" value="DELETE" style="width:auto;" onclick="if (confirm('Are you sure you want to delete this member?')) document.location.href='<?=str_replace('unit-list', 'unit-delete', $GLOBALS['redirect']).'&id='.$id.'&url='.$_REQUEST['url'];?>'" />
                  <? endif; ?>
                    
                    <input type="submit" class="btn btn-primary pull-right" value="SAVE" style="width:100px;" />
                </td>
            </tr>
        </table>
    </form>
</div>

<script type="text/javascript">
    $(function(){
        //quick styling using bottstrap button style
        $('input, select, textarea').addClass('btn btn-default')
            .not('.pnq, button, [type=button], [type=submit]').css('text-align', 'left');

        $('#description').on('change cut paste drop keydown', function() {
            $(this).css({
                'height': 'auto',
                'overflow': 'hidden',
                'white-space': 'pre-wrap'
            });
            this.style.height = this.scrollHeight+'px';
        }).attr('autocomplete', 'off').trigger('change');
    });
</script>
<style>
    td {
        padding-right: 15px;
        padding-bottom: 10px;
        vertical-align: text-top;
    }
    input, select, textarea {
        width: 100%;
    }
</style>
