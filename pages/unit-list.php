<h3 class="well">Data Lokasi Absensi</h3>

<a href="?p=unit-edit&url=<?=base64_encode('//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>" class="btn btn-success pull-right" style="height:34px; margin:-70px 15px;">
	<i class="fa fa-plus visible-xs"></i> <span class="hidden-xs">Add Data</span>
</a>

<div id="container">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-center" style="width:50px">ID</th>
					<th class="text-left">Lokasi</th>
					<th class="text-left">Deskripsi</th>
					<th class="text-center" style="width:150px">&nbsp;</th>
				</tr>
			</thead>
        </table>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		initDataTable('.table', $('.navbar-brand').attr('href') + '?d=units', 'undefined', 'undefined', [], [0, 'ASC']);
	});
</script>
<style type="text/css">
	.table [tabindex="0"] {
		text-align: center;
	}
</style>
