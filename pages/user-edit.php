<?php
    $msg = '';
    $GLOBALS['redirect'] = !empty($_REQUEST['url'])? base64_decode($_REQUEST['url']) : '?p=user-list';

    if (isset($_POST['id'])) { 
        $id         = $_POST['id'];
        $userid     = $_POST['userid'];
        $name       = $_POST['name'];
        $phone1     = $_POST['phone1'];
        $phone2     = $_POST['phone2'];
        $email1     = $_POST['email1'];
        $email2     = $_POST['email2'];
        $address    = $_POST['address'];
        $company    = $_POST['company'];
        $department = $_POST['department'];
        $position   = $_POST['position'];
        $locationid = $_POST['locationid'];
        $locationon = $_POST['locationonly'];
        
        // check to make sure required fields are entered
         if (trim($userid) == '' || trim($name) == '' || trim($email1) == '') {
            $msg = 'Please fill all the required fields!';
        }
        if ($msg == '') {
            $check = $db->row("SELECT * FROM users WHERE id != '{$id}' AND id = '{$userid}'");
            if ($check) {
                $msg = 'User ID already in use!';
            }
        }
        if ($msg == '') {
            $param = array(
                'userid' => $userid,
                'name' => $name,
                'phone1' => $phone1,
                'phone2' => $phone2,
                'email1' => $email1,
                'email2' => $email2,
                'address' => $address,
                'company' => $company,
                'department' => $department,
                'position' => $position,
                'locationid' => $locationid,
                'locationon' => $locationon
            );
            if (!empty($id)) {
                $db->query("UPDATE users SET id=:userid, name=:name, address=:address, phone=:phone1, phone2=:phone2, email=:email1, email2=:email2, company=:company, department=:department, position=:position, locationid=:locationid, locationonly=:locationon WHERE id = '{$id}';", $param); 
            } else {
                $db->query("INSERT users SET id=:userid, name=:name, address=:address, phone=:phone1, phone2=:phone2, email=:email1, email2=:email2, company=:company, department=:department, position=:position, locationid=:locationid, locationonly=:locationon;", $param);
            }

            if (trim($msg) == '')  {
                $msg = 'Data sucessfully saved!';
                echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$GLOBALS['redirect'].'">'; 
            }
        }
    } else {
        $id = isset($_GET['id'])? $_GET['id'] : '';
        $name = isset($_GET['name'])? $_GET['name'] : '';

        // if the form hasn't been submitted, display the form
        $result = $db->row("SELECT * FROM users WHERE id = '{$id}';");

        $userid     = $result? $result['id'] : $id;
        $name       = $result? $result['name'] : $name;
        $phone1     = $result? $result['phone'] : '';
        $phone2     = $result? $result['phone2'] : '';
        $email1     = $result? $result['email'] : '';
        $email2     = $result? $result['email2'] : '';
        $address    = $result? $result['address'] : '';
        $company    = $result? $result['company'] : '';
        $department = $result? $result['department'] : '';
        $position   = $result? $result['position'] : '';
        $locationid = $result? $result['locationid'] : '';
        $locationon = $result? $result['locationonly'] : '';

        if (!$result) {
            $id = '';
        }
    }
?>

<h3 class="well">
    <?=!empty($id)? 'Edit' : 'Tambah'; ?> Data Karyawan
</h3>
<div class="alert alert-<?= stripos($msg, 'sucessfully') !== false? 'success' : 'danger'; ?> col-md-12 <?= $msg == ''? 'hidden' : ''; ?>">
    <b class="<?= stripos($msg, 'sucessfully') !== false? 'hidden' : ''; ?>">ERROR:</b> <?= $msg; ?>
</div>

<div id="container">
    <form method="post" class="col-md-12">
        <input type="hidden" name="id" value="<?=$id;?>" />
        <table border="0" class="col-md-6">
            <tr>
                <td class="required" width="135px">User ID:</td>
                <td align="right">
                    <input type="text" name="userid" value="<?=$userid;?>" />
                </td>
            </tr>
            <tr>
                <td class="required" >Nama Karyawan:</td>
                <td align="right">
                    <input type="text" name="name" value="<?=$name;?>" />
                </td>
            </tr>
            <tr>
            <?php if (!empty($_SESSION['locs'])): ?>
                <input type="hidden" name="locationid" value="<?=$_SESSION['locs'];?>" />
            <?php else: ?>
                <td>Lokasi Karyawan:</td>
                <td align="right">
                    <select name="locationid">
                        <option value=""></option>
                    <?php foreach ($db->query("select * from units order by location") as $nt): ?>
                        <option <?=html_entity_decode($nt["locationid"])!=html_entity_decode($locationid)? '' : 'selected="selected"';?> value="<?=$nt["locationid"];?>"> <?=$nt["location"];?> </option>
                    <?php endforeach; ?>
                    </select>
                <?=mysql_error();?>
                </td>
            <?php endif; ?>
            </tr>
            <tr>
                <td>Alamat: </td>
                <td align="right">
                    <textarea id="address" name="address"><?=$address?></textarea>
                </td>
            </tr>
            <tr>
                <td>Telpon Utama: </td>
                <td align="right">
                    <input type="phone" name="phone1" value="<?=$phone1?>" />
                </td>
            </tr>
            <tr>
                <td>Telpon Alternatif: </td>
                <td align="right">
                    <input type="phone" name="phone2" value="<?=$phone2?>"/>
                </td>
            </tr>
            <tr>
                <td>Email Aktif: </td>
                <td align="right">
                    <input type="email" name="email2" value="<?=$email2;?>" />
                </td>
            </tr>
            <tr>
                <td class="required">Email Yakuza: </td>
                <td align="right">
                    <input type="email" name="email1" value="<?=$email1;?>" />
                </td>
            </tr>
            <tr>
                <td>Perusahaan: </td>
                <td align="right">
                    <input type="text" name="company" value="<?=$company;?>" />
                </td>
            </tr>
            <tr>
                <td>Departemen: </td>
                <td align="right">
                    <input type="text" name="department" value="<?=$department;?>" />
                </td>
            </tr>
            <tr>
                <td>Jabatan: </td>
                <td align="right">
                    <input type="text" name="position" value="<?=$position;?>" />
                </td>
            </tr>
            <tr class="admin" style="display:none;">
            <?php if (!empty($_SESSION['locs'])): ?>
                <input type="hidden" name="locationonly" value="<?=$locationon;?>" />
            <?php else: ?>
                <td>Lokasi Admin:</td>
                <td align="right">
                    <select name="locationonly">
                        <option <?=$locationon == 'Y'? 'selected' : ''; ?> value="Y">Self Location</option>
                        <option <?=$locationon != 'Y'? 'selected' : ''; ?> value="N">All Location</option>
                    </select>
                </td>
            <?php endif; ?>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <input type="button" class="btn btn-warning pull-left hidden" value="CANCEL" style="width:auto; margin-right:10px;" onclick="document.location.href='<?=$GLOBALS['redirect'];?>';" />
                  <? if (!empty($id)): ?>
                    <input type="hidden" name="url" value="<?=$_REQUEST['url'];?>" />
                    <input type="button" class="btn btn-danger pull-left" value="DELETE" style="width:auto;" onclick="if (confirm('Are you sure you want to delete this member?')) document.location.href='<?=str_replace('user-list', 'user-delete', $GLOBALS['redirect']).'&id='.$id.'&url='.$_REQUEST['url'];?>'" />
                  <? endif; ?>
                    
                    <input type="submit" class="btn btn-primary pull-right" value="SAVE" style="width:100px;" />
                </td>
            </tr>
        </table>
    </form>
</div>

<script type="text/javascript">
    $(function(){
        //quick styling using bottstrap button style
        $('input, select, textarea').addClass('btn btn-default')
            .not('.pnq, button, [type=button], [type=submit]').css('text-align', 'left');

        $('#address').on('change cut paste drop keydown', function() {
            $(this).css({
                'height': 'auto',
                'overflow': 'hidden',
                'white-space': 'pre-wrap'
            });
            this.style.height = this.scrollHeight+'px';
        }).attr('autocomplete', 'off').trigger('change');
    });
</script>
<style>
    td {
        padding-right: 15px;
        padding-bottom: 10px;
        vertical-align: text-top;
    }
    input, select, textarea {
        width: 100%;
    }
</style>
