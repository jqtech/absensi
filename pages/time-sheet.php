<h3 class="well">Data Absensi Harian</h3>
</a>

<div id="container">
	<div class="col-md-12">
		<?php
			$ymd_start_text = '';
            if (empty($_GET['date'])) {
                $_GET['date'] = date('Ymd', strtotime('monday this week'));
            } else {
            	$ymd_start_text = DateTime::createFromFormat('Ymd', $_GET['date'])->format('Y-m-d');
            	$ymd_start = DateTime::createFromFormat('Ymd', $_GET['date'])->format('Y-m-d');
            	$ymd_start = strtotime($ymd_start);
            	$_GET['date'] = date('Ymd', strtotime('monday this week', $ymd_start));
            }
            $ymd_start = DateTime::createFromFormat('Ymd', $_GET['date'])->format('Y-m-d');
            $time_start = strtotime($ymd_start);

            $ymd_text_end = '';
            if (empty($_GET['end'])) {
                $_GET['end'] = strtotime('+1 week -1 day',  $time_start);
                $_GET['end'] = date('Ymd', $_GET['end']);
            } else {
            	$ymd_text_end = DateTime::createFromFormat('Ymd', $_GET['end'])->format('Y-m-d');
            }
            $ymd_end = DateTime::createFromFormat('Ymd', $_GET['end'])->format('Y-m-d');
            $time_end = strtotime($ymd_end);

            $date_range = 'date='.date('Ymd', $time_start).'&end='.date('Ymd', $time_end);
        ?>
        <div class="pull-right">
            <label class="control-label" style="padding-right:10px;">
                Mingguan
            </label>
            <label for="cgroup" class="control-label">
                <a href="?p=time-week&<?= $date_range; ?>" data-toggle="tooltip" title="Ubah ke tampilan Mingguan"> <i class="fa fa-exchange"></i> </a>
            </label>
        </div>

		<div class="hidden">
			<input id="from" name="pstart" type="text" value="<?= $ymd_start_text; ?>" placeholder="From" class="datatable-filters form-control input-sm" />
			<input id="until" name="puntil" type="text" value="<?= $ymd_text_end; ?>" placeholder="Until" class="datatable-filters form-control input-sm" />
	  	</div>

		<table class="table table-striped table-bordered table-condensed" width="100%">
			<thead>
				<tr>
					<th width="10%">Tanggal</th>
					<th width="30%">Pegawai</th>
					<th width="20%">Aktifitas</th>
					<th width="20%">Lokasi</th>
					<th width="10%">IN</th>
					<th width="10%">OUT</th>
				</tr>
			</thead>
        </table>
    </div>
</div>

<script type="text/javascript">
	var datatables_draw = function() {
		var _tbl = $('.table').DataTable();
        var rows = _tbl.rows({page: 'current'}).nodes();

        /*
        var total = _tbl.column(5, {page:'current'} ).data().reduce(function (a, b) {
        	var cur_index = _tbl.column(5).data().indexOf(b);
            if (_tbl.column(8).data()[cur_index].toLowerCase() != "waiting payment") {
            	b = str.between('<span style="white-space:nowrap;">', '</span>', b);
	        	b = str.number(b.replace('Rp', '').replace(/,/g, ''));
	            return a + b;
            } else {
            	return a;
            }
        }, 0);
        $(_tbl.column(5).footer()).html(
            str.money(total).replace('Rp ', 'Rp')
        );
        */
	}
	var set_datepicker = function(obj) {
		var setdate = $(obj).datepicker('getDate');
		if (typeof setdate.setDate != 'undefined') setdate.setDate(setdate.getDate());
		$('#until').datepicker('option','minDate',setdate); 
	}
	jQuery(document).ready(function($) {
		$('#from, #until').on('change', function(){
			$('.table').DataTable().ajax.reload();
		}).datepicker({
			changeMonth: false,
			changeYear: true, 
            showOtherMonths: true,    // new code
            selectOtherMonths: true,  // new code
            firstDay: 1,               // new code
			minDate: new Date(2021, 1 - 1, 1), 
			dateFormat: "yy-mm-dd",
			onSelect: function(dateStr) {
				if ($(this).attr('id')!='until') set_datepicker(this);
				$(this).trigger('change');
			}
		}).attr('autocomplete', 'off');

		var searchparam = {};
	    $('.datatable-filters').each(function(){
	        searchparam[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
	    });
		initDataTable('.table', $('.navbar-brand').attr('href') + '?d=times', 'undefined', 'undefined', searchparam, [[0, 'DESC']]);

		$('#from').css('width', '100px').appendTo('.dataTables_filter');
		$('#until').css('width', '100px').appendTo('.dataTables_filter');

		$(document).on('click', '.table [tabindex="0"] xspan', function(){
			var that = this,
				status, callback;
			if ($(this).find('.fa').hasClass('fa-bookmark')) {
				status = 'Waiting Payment';
				callback = function(){
					$(that).find('.fa').removeClass('fa-bookmark').addClass('fa-bookmark-o');
				};
			} else {
				status = 'Payment Verified';
				callback = function(){
					$(that).find('.fa').removeClass('fa-bookmark-o').addClass('fa-bookmark');
				};
			}
			$.post(document.location.href, {id: $(this).text().trim(),status: status}, function(res){
				if (res === 'ok') {
					$('.table').DataTable().ajax.reload();
					callback();
				} else {
					alert(res);
				}
			});
		});
	});
</script>
<style type="text/css">
	.table [tabindex="0"] {
		xcursor: pointer;
		text-align: center;
	}
</style>
