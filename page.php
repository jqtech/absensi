<?php
    include "authorized.php";

    if (!empty($_GET['d'])) {
        include "pagedata/{$_GET['d']}.php";
        exit;
    }

    if (empty($_GET['p'])) {
        $_GET['p'] = 'default';
    }

    if (file_exists($page = "pages/{$_GET['p']}.php")) {
        ob_start();
        include $page;
        $content = ob_get_clean();
    } else {
        $content = '
<div class="alert alert-danger col-md-12">
    <b>Warning:</b> Page Not Found
</div>';
    }

    echo '<!DOCTYPE html>';
?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>AMS - Attendance Management System</title>

    <!-- jQuery -->
    <script src="https://thelegion.co.id/assets/js/jquery.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="https://thelegion.co.id/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://thelegion.co.id/assets/css/bootstrap.print.min.css" rel="stylesheet" />

    <!-- DatePicker Core CSS -->
    <link href="https://thelegion.co.id/assets/css/datepicker.css" rel="stylesheet" />

    <!-- DataTable Core CSS -->
    <link href="https://thelegion.co.id/assets/css/datatables.min.css" rel="stylesheet" />

    <!-- MetisMenu CSS -->
    <link href="./assets/metisMenu.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="./assets/sb-admin-2.css" rel="stylesheet" />
    <link href="./assets/util.css?20210813" rel="stylesheet" />
    <style>
        input {
            -webkit-user-select: text !important; /* Chrome, Opera, Safari */
            -moz-user-select: text !important; /* Firefox 2+ */
            -ms-user-select: text !important; /* IE 10+ */
            user-select: text !important; /* Standard syntax */
        }
        @media screen and (max-width: 767px) {
            /* Extra small devices: phone */
        }
        @media screen and (min-width: 768px) and (max-width: 991px) {
            /* Small devices: tablets */
        }
        @media screen and (min-width: 992px) and (max-width: 1199px) {
            /* Small devices: desktops */
        }
        @media screen and (min-width: 1200px) {
            /* Large devices: large desktops */
        }
        @media screen and (max-width: 767px) {
            .navbar-header {
                height: 15vh;
            }
            .sidebar-nav {
                position: relative !important; 
                height: 85vh;
            }
        }
        @media screen and (min-width: 768px) {
            .sidebar {
                position: fixed !important;
                height: 100%;
                height: -webkit-fill-available;
                overflow-y: auto;
                overflow-x: hidden;
            }
        }

        .required:after {
            content: '*';
            color: red;
            margin-left: 5px;
        }

        .well {
            margin-top: 0;
        }

        .a-pointer,
        .imgpreview,
        .printpreview {
            cursor: pointer;
        }
    </style>

    <!-- Custom Fonts -->
    <link href="https://thelegion.co.id/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Custom Icons -->
    <link href="../assets/img/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="position: fixed; top: 0; width: 100%; margin-bottom: 0; background-color: #fff;">
            <div class="hidden-xs pull-right" style="padding: 15px 15px;">
                Hello, <?= @$_SESSION['auth']; ?>
            </div>
            <a class="navbar-brand" href="./" style="color: #333;">
                <span class="hidden-xs">AMS &ndash;</span> Attendance Management System
            </a>
            <div class="navbar-header" style="height: auto;">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- /.navbar-header -->
            <div class="navbar-default sidebar" role="navigation" style="background-color: #FFF;">
                <div class="sidebar-nav navbar-collapse collapse" style="width: inherit;">
                    <ul class="nav" id="side-menu" style="width: inherit;">
                        <li>
                            <a href="?p=user-list"; style="color: #333;">
                                <i class="fa fa-user fa-fw"></i> Pegawai
                            </a>
                        </li>
                        <li>
                            <a href="?p=time-sheet"; style="color: #333;">
                                <i class="fa fa-clock-o fa-fw"></i> Absensi
                            </a>
                        </li>
                        <?php if (empty($_SESSION['loc'])): ?>
                        <li>
                            <a href="?p=unit-list"; style="color: #333;">
                                <i class="fa fa-building fa-fw"></i> Lokasi
                            </a>
                        </li>
                        <li>
                            <a href="?p=reason-list"; style="color: #333;">
                                <i class="fa fa-comment fa-fw"></i> Aktifitas
                            </a>
                        </li>
                        <?php endif; ?>
                        <li>
                            <form id="mailbox" class="hidden" method="post" action="https://mail.yakuza.co.id/auth.php" target="_blank">
                                <input type="hidden" name="email" value="<?= @$_SESSION['auth']; ?>" />
                                <input type="hidden" name="passw" value="<?= @$_SESSION['pass']; ?>" />
                            </form>
                            <a href="javascript:$('#mailbox').submit()"; style="color: #333;">
                                <i class="fa fa-envelope fa-fw"></i> Mailbox
                            </a>
                        </li>
                        <li>
                            <a href="?p&logout"; style="color: #333;">
                                <i class="fa fa-sign-out fa-fw"></i> Logout
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.sidebar -->
        </nav>
        <!-- /.navbar-static-top -->
        
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid" style="padding: 60px 0 0 0;">
                <?= @$content; ?>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- #pagepreview -->
    <div id="pagepreview" class="modal fade" style="overflow:hidden;" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body container" style="width:inherit; height:70vh;">
                    <iframe src="" style="border:0; width:100%; height:100%; overflow-x:hidden;"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /#pagepreview -->

    <!-- #printpreview -->
    <div id="printpreview" class="modal fade" style="overflow:hidden;" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Print Preview</h4>
                </div>
                <div class="modal-body container" style="width:inherit; height:70vh; background:#fff url(&quot;data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='50px' width='120px'><text x='0' y='15' fill='grey' font-size='20'>Loading...</text></svg>&quot;) center center no-repeat;">
                    <iframe src="" style="border:0; width:100%; height:100%;"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" onclick="$('#printpreview iframe')[0].contentWindow.print();">Print</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /#printpreview -->

    <!-- #imgpreview -->
    <div id="imgpreview" class="modal fade" tabindex="-1" role="dialog" data-keyboard="true" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Image Preview</h4>
                </div>
                <div class="modal-body container" style="width:inherit;">
                    <img src="" style="max-width:100%; height:auto;" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /#imgpreview -->

    <!-- Bootstrap Core JavaScript -->
    <script src="https://thelegion.co.id/assets/js/bootstrap.min.js"></script>

    <!-- DatePicker Core JavaScript -->
    <script src="https://thelegion.co.id/assets/js/datepicker.js"></script>

    <!-- DataTable Core JavaScript -->
    <script src="https://thelegion.co.id/assets/js/datatables.min.js?mod2"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./assets/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="./assets/sb-admin-2.js"></script>

    <!-- Custom Javascript -->
    <script src="./assets/util.js?20210813"></script>
    <script type="text/javascript">
        $(function() {
            var title = $('#page-wrapper h3:eq(0)').text();
            if ($.trim(title) != '') document.title = 'AMS - ' + title;

            $(document).on('click', '.imgpreview', function() {
                $('#imgpreview img').attr('src', $(this).attr('value'));
                $('#imgpreview').modal('show');
            });
            $(document).on('click', '.printpreview', function(e) {
                $('#printpreview iframe').attr('src', 'about:blank');
                $('#printpreview iframe').attr('src', $(this).attr('href'));
                $('#printpreview').modal('show');
                e.preventDefault();
            });
            $(document).on('click', '.pagepreview', function(e) {
                $('#pagepreview iframe').attr('src', 'about:blank');
                $('#pagepreview iframe').attr('src', $(this).attr('href').replace('?p=', '?w='));
                $('#pagepreview').modal('show');
                e.preventDefault();
            });

            $('#pagepreview iframe').on('load', function() {
                if ($(this).attr('src') == '') return;
                $('#pagepreview iframe')[0].contentDocument.body.style.overflowX = 'hidden';
                $('#pagepreview').modal('show');

                $('#pagepreview iframe')[0].contentWindow.$($('#pagepreview iframe')[0].contentDocument).on('click', '.imgpreview', function() {
                    $('#imgpreview img').attr('src', $(this).attr('value'));
                    $('#imgpreview').modal('show');
                });
                $('#pagepreview iframe')[0].contentWindow.$($('#pagepreview iframe')[0].contentDocument).on('click', '.printpreview', function() {
                    $('#printpreview img').attr('src', $(this).attr('value'));
                    $('#printpreview').modal('show');
                });
            });
            $('#pagepreview').on('hide.bs.modal', function() {
                document.location.reload();
            });

            $('textarea').each(function() {
                autoSizeTextarea(this);
            });

            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>
