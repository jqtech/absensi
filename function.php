<?php
	require_once("database.pdo.php");
	require_once("database.php");
	
	function createDataTable($pdo, $aColumns, $sIndexColumn, $sTable, $join = array(), $where = array(), $additionalSelect = array(), $orderby = '', $groupBy = '') {
	    /*
	     * Paging
	     */
	    $sLimit = "";
	    if ((is_numeric($_POST['start'])) && $_POST['length'] != '-1') {
	        $sLimit = "LIMIT " . intval($_POST['start']) . ", " . intval($_POST['length']);
	    }
	    $_aColumns = array();
	    foreach ($aColumns as $column) {
	        // if found only one dot
	        if (substr_count($column, '.') == 1 && strpos($column, ' as ') === false) {
	            $_column = explode('.', $column);
	            if (isset($_column[1])) {
	                array_push($_aColumns, $column);
	            } else {
	                array_push($_aColumns, $_column[0]);
	            }
	        } else {
	            array_push($_aColumns, $column);
	        }
	    }
	    /*
	     * Ordering
	     */
	    $sOrder = "";
	    if ($_POST['order']) {
	        $sOrder = "ORDER BY  ";
	        foreach($_POST['order'] as $key=>$val){

	        $sOrder .= $aColumns[intval($_POST['order'][$key]['column'])];

	        $__order_column = $sOrder;
	        if (strpos($__order_column, ' as ') !== false) {
	            $sOrder = string_before($__order_column, ' as');
	        }
	        $_order = strtoupper($_POST['order'][$key]['dir']);
	        if ($_order == 'ASC') {
	            $sOrder .= ' ASC';
	        } else {
	            $sOrder .= ' DESC';
	        }
	        $sOrder .= ', ';
	        }
	        if (trim($sOrder) == "ORDER BY") {
	            $sOrder = "";
	        }
	        if ($sOrder == '' && $orderby != '') {
	            $sOrder = $orderby;
	        } else {
	             $sOrder = substr($sOrder,0,-2);
	        }

	    } else {
	        $sOrder = $orderby;
	    }
	    /*
	     * Filtering
	     * NOTE this does not match the built-in DataTables filtering which does it
	     * word by word on any field. It's possible to do here, but concerned about efficiency
	     * on very large tables, and MySQL's regex functionality is very limited
	     */
	    $sWhere = "";
	    if ((isset($_POST['search'])) && $_POST['search']['value'] != "") {
	        $search_value = $_POST['search']['value'];

	        $sWhere = "WHERE (";
	        for ($i = 0; $i < count($aColumns); $i++) {
	            $__search_column = $aColumns[$i];
	            if (strpos($__search_column, ' as ') !== false) {
	                $__search_column = string_before($__search_column, ' as');
	            }
	            if (isset($_POST['columns']) && isset($_POST['columns'][$i]) && $_POST['columns'][$i]['searchable'] == "true") {
	                $sWhere .= $__search_column . " LIKE '%" . $search_value . "%' OR ";
	            }
	        }
	        if (count($additionalSelect) > 0) {
	            foreach ($additionalSelect as $searchAdditionalField) {
	               if (strpos($searchAdditionalField, ' as ') !== false) {
	                $searchAdditionalField = string_before($searchAdditionalField, ' as');
	            }

	            $sWhere .= $searchAdditionalField . " LIKE '%" . $search_value . "%' OR ";
	        }
	    }
	    $sWhere = substr_replace($sWhere, "", -3);
	    $sWhere .= ')';
	} else {
	        // Check for custom filtering
	    $searchFound = 0;
	    $sWhere      = "WHERE (";
	    for ($i = 0; $i < count($aColumns); $i++) {
	        if (isset($_POST['columns']) && isset($_POST['columns'][$i]) && $_POST['columns'][$i]['searchable'] == "true") {
	            $search_value = $_POST['columns'][$i]['search']['value'];
	            $__search_column = $aColumns[$i];
	            if (strpos($__search_column, ' as ') !== false) {
	                $__search_column = string_before($__search_column, ' as');
	            }
	            if ($search_value != '') {
	                $sWhere .= $__search_column . " LIKE '%" . $search_value . "%' OR ";
	                if (count($additionalSelect) > 0) {
	                    foreach ($additionalSelect as $searchAdditionalField) {
	                        $sWhere .= $searchAdditionalField . " LIKE '%" . $search_value . "%' OR ";
	                    }
	                }
	                $searchFound++;
	            }
	        }
	    }
	    if ($searchFound > 0) {
	        $sWhere = substr_replace($sWhere, "", -3);
	        $sWhere .= ')';
	    } else {
	        $sWhere = '';
	    }
	}

	    /*
	     * SQL queries
	     * Get data to display
	     */
	    $_additionalSelect = '';
	    if (count($additionalSelect) > 0) {
	        $_additionalSelect = ',' . implode(',', $additionalSelect);
	    }
	    $where = implode(' ', $where);
	    if ($sWhere == '') {
	        if (string_start_With($where, 'AND') || string_start_With($where, 'OR')) {
	            if(string_start_With($where, 'OR')){
	               $where = substr($where, 2);
	            } else {
	                $where = substr($where, 3);
	            }
	            $where = 'WHERE ' . $where;
	        }
	    }
	    $sQuery         = "
	    SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(", ", $_aColumns)) . " " . $_additionalSelect . "
	    FROM   $sTable
	    " . implode(' ', $join) . "
	    $sWhere
	    " . $where . "
	    $sOrder
	    $groupBy
	    $sLimit
	    ";

	    $rResult = array();
	    foreach ($pdo->query($sQuery) as $rData) {
	    	$rResult[] = $rData;
		}

	    /* Data set length after filtering */
	    $sQuery         = "
	    SELECT FOUND_ROWS() as num_rows
	    ";
	    $cResult = $pdo->row($sQuery);
	    $iFilteredTotal = $cResult['num_rows'];
	    if (string_start_With($where, 'AND')) {
	        $where = 'WHERE ' . substr($where, 3);
	    }
	    /* Total data set length */
	    $sQuery = "
	    SELECT COUNT(" . $sTable . '.' . $sIndexColumn . ") as num_rows
	    FROM $sTable " . implode(' ', $join) . ' ' . $where;
	    $cResult = $pdo->row($sQuery);
	    $iTotal = $cResult['num_rows'];
	    /*
	     * Output
	     */
	    $output = array(
	        "draw" => isset($_POST['draw']) ? intval($_POST['draw']) : 0,
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iFilteredTotal,
	        "aaData" => array()
	        );
	    return array(
	        'rResult' => $rResult,
	        'output' => $output
	        );
	}

	function filterDataTable($filter) {
	    $filter = implode(' ', $filter);
	    if (string_start_With($filter, 'AND')) {
	        $filter = substr($filter, 3);
	    } else if (string_start_With($filter, 'OR')) {
	        $filter = substr($filter, 2);
	    }
	    return $filter;
	}

	function is_mobile() {
	    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", @$_SERVER["HTTP_USER_AGENT"]);
	}

	function number_to_alphabet($number) {
	    $alphabet = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
	    $count = count($alphabet);
        if ($number <= $count) {
            return $alphabet[$number - 1];
        }
        $alpha = '';
        while ($number > 0) {
            $modulo = ($number - 1) % $count;
            $alpha  = $alphabet[$modulo] . $alpha;
            $number = floor((($number - $modulo) / $count));
        }
	    return $alpha;
	}

	function number_to_roman($number) {
	    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
	    $returnValue = '';
	    while ($number > 0) {
	        foreach ($map as $roman => $int) {
	            if($number >= $int) {
	                $number -= $int;
	                $returnValue .= $roman;
	                break;
	            }
	        }
	    }
	    return $returnValue;
	}

	function number_to_decimal($number, $separator = null, $decimal = null, $nozerodecimal = false) {
	    if (is_null($separator)) {
			$separator = ',';
		}
	    if (is_null($decimal)) {
			$decimal = 2;
		}
		$number = number_format($number, $decimal, '.', $separator);
		if ($nozerodecimal) {
		    $number = str_replace('.' . str_repeat('0', $decimal), '', $number);
		}
		return $number;
	}

	function number_to_percent($number, $sign = null, $empty = 0, $decimal = 2, $nowrap = true) {
		if (is_null($sign)) {
			$sign = '%';
		}
		if (empty($number)) {
			$number = $empty;
			if ($empty === '') {
				$sign = '';
			}
		}
		if ($number === '') {
			$sign = '';
		} else {
			$number = number_to_decimal($number, '', $decimal, $empty === 0);
		}
		if (!$nowrap) {
			return $number . $sign;
		}
		return '<span style="white-space:nowrap;">' . $number . $sign . '</span>';
	}

	function number_to_money($number, $sign = null, $empty = 0, $decimal = 2, $nowrap = true) {
		if (is_null($sign)) {
			$sign = 'Rp ';
		}
		if (empty($number)) {
			$number = $empty;
		}
		if ($number === '') {
			$sign = '';
		} else {
			$number = number_to_decimal($number, null, $decimal, false);
		}
		if (!$nowrap) {
			return $sign . $number;
		}
		return '<span style="white-space:nowrap;">' . $sign . $number . '</span>';
	}

	if (!function_exists('wordwrap_array')) {
		function wordwrap_array($longString='', $maxLineLength=100) {
			$arrayWords = explode(' ', $longString);

			$index = 0;
			$currentLength = 0;
			$arrayOutput = array();

			foreach ($arrayWords as $word) {
				$wordLength = strlen($word) + 1;

				if (!isset($arrayOutput[$index])) {
					$arrayOutput[] = '';
				}

				if ( ($currentLength + $wordLength) <= $maxLineLength ) {
					$arrayOutput[$index] .= $word . ' ';

					$currentLength += $wordLength;
				} else {
					$index += 1;
					$currentLength = $wordLength + 2;
					$arrayOutput[$index] = ' ' . $word;
				}

				if (empty($arrayOutput[$index])) {
					unset($arrayOutput[$index]);
				}
			}
			
			return $arrayOutput;	
		}
	}

	if (!function_exists('hour_add')) {
		function hour_add($hour = '0:00', $addh = 0) {
			$hour = (strpos($hour,':')!==false? $hour : intval($hour).':00').':00';
			$addh = (strpos($addh,':')!==false? $addh : intval($addh).':00').':00';
			
			$totalseconds = 0;
			list($hours,$minutes,$seconds) = explode(':',$hour);
			$totalseconds += ($hours * 60 * 60) + ($minutes * 60) + $seconds;
			list($hours,$minutes,$seconds) = explode(':',$addh);
			$totalseconds += ($hours * 60 * 60) + ($minutes * 60) + $seconds;
			
			$hours = floor($totalseconds / 3600);
				if ($hours < 10) $hours = '0'.$hours;
			$minutes = floor(($totalseconds / 60) % 60);
				if ($minutes < 10) $minutes = '0'.$minutes;
			$seconds = $totalseconds % 60;
				if ($seconds < 10) $seconds = '0'.$seconds;
			
			return "{$hours}:{$minutes}";
		}
	}

	if (!function_exists('last_monday')) {
		function last_monday($date) {
			if (!is_numeric($date)) {
				$date = strtotime($date);
			}
			if (date('w', $date) == 1) {
				return $date;
			} else {
				return strtotime('last monday', $date);
			}
		}
	}

	if (!function_exists('datetime_parse')) {
		function datetime_parse($string, $default = 0, $strict = false) {
			preg_match('/^(\d{1,2})[\/|-](\d{1,2})[\/|-](\d{4})[\s]?(\d{1,2})?[:]?(\d{1,2})?[:]?(\d{1,2})?/i', $string, $match);

			if (empty($match) && $strict) {
				return $default;
			}

			$day 	= isset($match[1])? $match[1]: (!empty($default)? date('d', $default): 0);
			$month 	= isset($match[2])? $match[2]: (!empty($default)? date('m', $default): 0);
			$year 	= isset($match[3])? $match[3]: (!empty($default)? date('Y', $default): 0);
			
			$hour 	= isset($match[4])? $match[4]: (!empty($default)? date('H', $default): 0);
			$minute = isset($match[5])? $match[5]: (!empty($default)? date('i', $default): 0);
			$second = isset($match[6])? $match[6]: (!empty($default)? date('s', $default): 0);

			$stamp = array(
				'datetime' => mktime($hour, $minute, $second, $month, $day, $year),
				'date' => mktime(0, 0, 0, $month, $day, $year),
				'time' =>mktime($hour, $minute, $second, 0, 0, 0)
			);
			
			return array(
				'year' => $year,
				'month' => $month,
				'day' => $day,
				'hour' => $hour,
				'minute' => $minute,
				'second' => $second,
				'stamp' => $stamp
			);
		}
	}

	if (!function_exists('date_to_timestamp')) {
		function date_to_timestamp($string, $default = 0) {
			$parse = datetime_parse($string, $default, true);
			return is_array($parse) && isset($parse['stamp'])? $parse['stamp']['date']: $default;
		}
	}

	if (!function_exists('money_format')) {
		function money_format($input, $decimal = 3, $append = '$ ', $prepend = '') {
			return $append.number_format(floatval($input), $decimal, '.', '').$prepend;
		}
	}

	if (!function_exists('money_compare')) {
		function money_compare($input1, $input2, $decimal = 3) {
			return money_format($input1, $decimal, '') == money_format($input2, $decimal, '');
		}
	}

	if (!function_exists('first_text')) {
		function first_text($string, $delimiter = ' '){
			$ret = explode($delimiter,trim($string));
			return $ret[0];
		}
	}

	if (!function_exists('color_random')) {
		function color_random(&$group = null, $by = ''){
			if (empty($group)) $group = array();
			if (empty($group[$by])) {
				$r = mt_rand(100, 255);
			  $g = mt_rand(100, 255);
			  $b = mt_rand(100, 255);
				$color = sprintf('#%02X%02X%02X', $r, $g, $b);
				//$color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
				$find = array_search($color, $group);
				$group[$by] = empty($find)? $color: color_random($group, $by);
			}
			
			return $group[$by];
		}
	}

	if (!function_exists('string_between')) {
		function string_between($start, $end, $var, $icase = false, $array = false) {
			$return = preg_match_all('{' . preg_quote($start) . '(.*?)' . preg_quote($end) . '}s'.($icase?'i':''), $var, $m);
			if ($array) {
				return $return? $m[1] : array();
			}
			return $return? current($m[1]) : '';
		}
	}

	if (!function_exists('string_start_With')) {
	    function string_start_With($haystack, $needle) {
	        // search backwards starting from haystack length characters from the end
	        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	    }
	}

	if (!function_exists('string_end_With')) {
	    function string_end_With($haystack, $needle)
	    {
	        // search forward starting from end minus needle length characters
	        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
	    }
	}

	if (!function_exists('string_after')) {
		function string_after($string, $substring) {
		    $pos = strpos($string, $substring);
		    return $pos === false? $string : (substr($string, $pos + strlen($substring)));
		}
	}

	if (!function_exists('string_before')) {
	    function string_before($string, $substring) {
		    $pos = strpos($string, $substring);
		    return $pos === false? $string : (substr($string, 0, $pos));
		}
	}

	if (!function_exists('file_url')) {
		function file_url($filename = false) {
			$url = explode('/', $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			$name = array_pop($url);
			return implode('/', $url).($filename? '/'.$name: '');
		}
	}

	if (!function_exists('var_dump_str')) {
		function var_dump_str($var, $print = true) {
			$var = nl2br(str_replace(' ','&nbsp;',var_export($var, true)) ).'<br/>';
			if ($print) print $var;
			return $var;
		}
	}

	if (!function_exists('do_curl')) {
		function do_curl($link, $data = null){
			set_time_limit(0);
			$ch = curl_init($link);  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
			curl_setopt($ch, CURLOPT_TIMEOUT, 600);  
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			if (!empty($data)) {		
				curl_setopt($ch, CURLOPT_POST, 1);		
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);	
			}	  
			$return = curl_exec($ch);
			$return = $return? $return : '';
			return $return;
		}
	}
