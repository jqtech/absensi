<?php
    require_once "function.php";

    // ajax user search
    if (isset($_REQUEST['find'])) {
        $res = $db->query("SELECT id, name FROM users WHERE id LIKE :id OR name LIKE :nm UNION SELECT username, 'GUEST' FROM timeclock WHERE userid = 'GUEST' AND username LIKE :gs AND whenout IS NULL", array(
            'id' => "{$_REQUEST['find']}%",
            'nm' => "%{$_REQUEST['find']}%",
            'gs' => "%{$_REQUEST['find']}%"
        ));
        echo json_encode($res);
        exit;
    }

    // remember email
    if (!empty($_REQUEST['mail'])) {
        setcookie('terminalmail', $_REQUEST['mail'],  time()+(365 * 24 * 60 * 60), '/');
    }
    // show register terminal
    if (!empty($_POST['terminalloc'])) {
        setcookie('terminalloc', $_POST['terminalloc'],  time()+(365 * 24 * 60 * 60), '/');
        setcookie('terminalnum', $_POST['terminalnum'],  time()+(365 * 24 * 60 * 60), '/');
        header('Location: ./');
        exit;
    }
    if (empty($_COOKIE['terminalloc']) || empty($_COOKIE['terminalnum'])) {
        if (@$_SESSION['auth'] != 'itdepartment@yakuza.co.id' && @$_SESSION['auth'] != 'humanresource@yakuza.co.id' && @$_SESSION['auth'] != 'security@yakuza.co.id') {
            header('Location: ./auth.php?err[]=IT/HR Department Login Required!');
        } else {
        $loc_error = isset($_POST['terminalloc']) && empty($_POST['terminalloc'])? 'Set Unit Location ID': (!empty($loc_error)? $loc_error: '');
        ?>
        <div style="width:100%; margin-top:50px;"  align="center">
            <form method="post" style="width:300px; padding:0 25px; border:double 3px #555;" align="left">
                <center><h2>UNIT ABSENSI</h2></center>
                <hr style="border:double 3px #555;" />
                <select type="text" name="terminalloc" value="<?=isset($_POST['terminalloc'])? $_POST['terminalloc']: @$_COOKIE['terminalloc'];?>" placeholder="LOCATION ID"style="width:100%;">
                    <option value="KRC">KERINCI</option>
                    <option value="WDT">WENDIT</option>
                    <option value="BRT">BRATAN</option>
                    <option value="BSR">BASRA</option>
                </select>
                <input type="text" name="terminalnum" value="<?=isset($_POST['terminalnum'])? $_POST['terminalnum']: @$_COOKIE['terminalnum'];?>" placeholder="UNIT NUMBER" style="width:100%;" />
                <div align="center" style="color:red; font-weight:bold;">
                    <?=$loc_error;?>
                </div>
                <p align="center">
                    <input type="submit" value="CONTINUE" style="font-weight:bold;" />
                </p>
            </form>
        </div>
        <?
        }   
        exit;
    }

    // request admin pages
    if (isset($_GET['p']) || isset($_GET['d'])) {
        require_once("page.php");
        exit;
    }

    // record sign in/out
    if (!empty($_POST['user'])) {
        $userid = $_POST['user'];
        $username = '';

        $param = array(
            'id' => $userid,
            'nm' => $userid,
            'loc' => $_COOKIE['terminalloc'],
        );
        if (!$data = $db->row("SELECT * FROM timeclock WHERE (userid=:id OR username=:nm) AND (locationin=:loc OR 1=1) AND whenout IS NULL;", $param)) {
            preg_match("/([0-9]+)/", $userid, $scanner);
            if ($scanner && is_numeric($scanner[1])) {
                $username = trim(str_replace($scanner[1], '', $userid));
                $userid = $scanner[1];
            }
        }

        if ($data) {
            header("Location: sign.php?out=".$data['timeid']);
            exit;
        } else {
            unset($param['nm'], $param['loc']);
            $data = $db->row("SELECT * FROM users WHERE id=:id;", $param);

            if (!$data) {
                $data = array(
                    'id' => !empty($username)? $userid: 'GUEST',
                    'name' => !empty($username)? $username: $userid,
                    'reason' => null
                );
            } else {
                $data['reason'] = 'Masuk Kerja';
            }

            $param = array(
                'id' => $data['id'],
                'name' => trim($data['name']),
                'reason' => $data['reason'],
                'loc' => $_COOKIE['terminalloc'],
                'unit' => $_COOKIE['terminalnum']
            );
            $id = $db->query("INSERT timeclock SET userid=:id, username=:name, reason=:reason, locationin=:loc, unitin=:unit, whenin=NOW();", $param);
            header("Location: sign.php?id=".$id);
            exit;
        }
    }

    echo '<!DOCTYPE html>';
?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>AMS - Attendance Management System</title>

    <link href="https://thelegion.co.id/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://thelegion.co.id/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/img/favicon.ico" rel="shortcut icon" />
    <style type="text/css">
        ::-webkit-scrollbar {
            display: none;
        }
        .form-signin
        {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-control
        {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus
        {
            z-index: 2;
        }
        .form-signin input[type="password"]
        {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="submit"]
        {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .form-signin input[type="text"], .form-signin select {
            border-radius: 0;
            margin-bottom: -1px;
        }
        .form-signin input::-webkit-input-placeholder {
            position: absolute;
            left: 55%;
            top: 13px;
        }
        .form-signin input:focus {
            border-color: #ccc;
        }
        .account-wall
        {
            margin-top: 24px;
            padding: 40px 0px 20px 0px;
            background-color: #fff;
            box-shadow: 0 5px 20px 0 rgba(0, 0, 0, 0.15);
        }
        .profile-title {
            border-bottom: 3px double #333;
            width: 90%;
            width: fit-content;
            margin: auto;
            padding-bottom: 8px;
            font-size: 16pt;
        }
        .profile-img
        {
            width: auto;
            max-height: 100px;
            margin: 0 auto 20px;
            display: block;
        }
        .alert-image
        {
            background-size: 100% 100%;
            background-repeat: no-repeat;
            background-position: top;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAMAAAAOusbgAAACAVBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///+T5UK1AAAAqnRSTlMAAQIDBAUGBwgJCgwNDg8QERITFBUWGBkaGxwdHh8gISIjJSYnKCkrLC0uLzAxMjM0NTc4Ozw9Pj9AQUJDREVGR0lKS0xNTk9RUlNUVVhZWltcXV5fYGFiY2RlZmdpamtsbm9wcXJzdnd4eXp8fX5/gIGChIWHiIqLjI6PkZKTlJWWl5iZmpucnZ6foKGio6Smp6ipqqusra6vsLGys7S1tre4ubq7vL2+v8CjqRgAAAQpSURBVHgB7dr7XxRVGAbwZ9ZVYiHXSNJQShZFMi9FJpWXLppBRlqGpXnpYqbbhawVjaxMMuwSmaYEUiCysO9/Wdra887OZ1b27Dnz03x/ct8zO68we/ac53xALBaLxWKxWOXmd/S82plC1LyuUfnX5P4FiFQyK0UXGhAh75T876cUorNXlM8Qmda8aJsRkcQP4jN+L6KxQ0q8g0jUjUqJmYcRhUMS8CUisCwvQU/APU5h5bf5cG29UIH/7IZjyV/YrF897Yn74NbL7JVfnhrhq2NwauE4Wx3xzejCSrj0PjuN1QHeIF9/48GdFbNs9BIAtBdYeBruDLDNUAK39LFypQaubBJah9sap1h6HY4suMQmJ1G0j7WpRrixhz1uLkFRzRVWP4ITDZNssZ/lrawWHoELJ9jhWi3L3lnWv/dg3yo1c56FklEDz8E67xxv/50H7ThH/kzBtm3qUbbBZ9GEfviW3XOVN8+ixG6O5ZtglZ6uk/ejRHKYo5/Dqgdu8tavlf9Keww29d1ll3NGRZp5sOdRoU4gqHmGF+x0Ex2+Cl2oGSxcRIeZhwA8Lz4HANRfV1sTF9HhKAA8Lj5dANCt/3P2o8P1egBYIT6bAGDeRevBQkeHXbglLT5tgQ13h9XowKni5UVrxG05y8GiQ2gD/vOHKLPFibt0mrVX7EaHUygaFGUERQetBotdvNv0gyjKiXIBRalrLH5QdXT4izc7hDuOiZLDHdtZLLTaiw4jXOR7ReEP5523FixaVHTYwfJOUfayvrrA8lO2osOgx/JGUbaDPtXBwk50KLSDVonSAVp8Q6/bVqJDH5RGUVqg9FoJFnvC7pIsCKWh1FzmQNZGdNgHnzE1uz1oWywEixPhn5QfOfQ7/KoPFm3q97k1fI/1LfwyOnFUGx3OevDLBj519KEOFlVGhwxKvF3mBDX9NwffQsVqVXQ4Xm7p6EGpnsC6Uok31Rq3CKWe4eg2lAsWJ6uJDrsRsEYfhgRsFFprHh2Gkwho4vAyBJ02DhZrhZ5EUC2HUwhqznP8RVQgMcQ3noGBd/n+0TrD6NAMA/XqO/WwWXR4D0a6jILFYX90MJFQwaLfJDp0w9A6g2ChosPFsMnASxDiC17ya7Li6LAexo2X6GBRaXTIwaCxabDQ0WGpSWPDYJFW0eEgqmmsDw5mW3A3R9VRaaq6xjpYfO1VEB1egHnjQLDonHt0OO9V2xgf87JLC+YcHVbDrLFJsNDR4RNU3xhvqCPQhrlFhxuLYcggWOjo0AsrNoccc4euSldr7R/s9yPEwikHf2KRUfMzAwo5aD/nwZZs2P6cBsWx8QSIkjPiWhOI0uJcO4hS4lwrFLosjuVTYVsGx3LQKD0mTuVbEGLNhNO+WxBq+YA4M9SOclYeOP3zlFg2PTxwZIMHLRaLxWKxGP0DJcnmTDeTXUEAAAAASUVORK5CYII=);
        }
        .lock-image 
        {
            background-size: 100% 100%;
            background-repeat: no-repeat;
            background-position: center;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAQAAACTbf5ZAAALYElEQVR42u2deZAU1RnAp6gkGlJSUaISRVLmWEgMEkIBFoQjXFnZuDvvfd/X/QZTWqXcgRI0QRRCICIJREM4w2Wh3IqABSrnyiKbisjhci4Jyrkcq5y7sMzs9fLH7MK81z1H987sHGX3fz093e/3Xe+9r/t97fF8vSVmy72X/Qx6cR8M4X+GaTAVpsI0mMyH8Kd4X/aIcX9GQGZ9g3WDoXwRFuBhvGRKEWY3JV7B/2IhX85G8Z7wvTREhW4wDrfTcaqpRzKlEWE3Zb04SGIJFsKr/DdpgsqzYS4eJX8Q03CxB9GpGr/gb3p5CqP2bc6m4lGqcgtqB461eBpm5/445WC9XXA1VcQLVcemAOTznJSBZdnwiSFF3FFDdyGFxENcJB+2GxSaUWFDQxJVUoAqjDIqpxsUoADK2MJaHfRBnjyvznsQ1lJE2CAm+ukC7IdVOBVf4IJ3Ye3zHvZ4PB7enLVnnTng8/BXWAJ76TzeiBbqhDQlFPAOyeh4/kSXRURtkB+P8XdhNOses710gmH8DSrG60JGujb5YUbjdj1d4NNwmjClkBjAIvgb+5Vr2/klvIyFdD3SPaC40cIYH09lIryXnYQZ7LG4RP62/FU8HC5CCEmVMLcxTPl9+/AipJC4BwbGPSwyyKew0LA7994Ewua0xFMiDCzs4j0SJuR2sMle00LiVW+/RA0uAAMijD+BN+GzrY7wiZ1tmdKQMCoRUv4j1Zp2Er4CExqt1x8OZ+31zGfFO1BNNyzSNaUhYRNr1agdRBNYSjV2godV8dTuTFvdlvHRSZl6CjwvEokcBvcA+0Wyxnj974f8hCHzV2wN6N2kz7tnk8XJhOQrGqrd4bae+/eUyKqMpEob5Ncb0hH1JAsu1bKGdwLfpIewZe4DDY7aeViuI/skG+r2cq3wqqnj+jm51kgfPou24gmqMmX9DhdhN7zNR3hdZi1ZJ7yqezPWsN6uLob7hY4bcDcbZd1hGZTWz3utE0ghMYAF8HtPExcTjc54Tb8mnmEtnOtjiS45knyQq6TeFqoRsU3vT/NJrtJLN0w9qG532sxBVKt7L7zktClPNIWVVCWcpe2OOE/kgI+qLMhTnOB+H89aLjDfsW5z8LRwkb2iWu78XmMswfV67IkHD6wVOm6+Y78dQjVuE3s+iR811AWFxN2xSutJy6iqJPe7DnHHhU/KmcpDlnB6xh2OkT/VkfnE2KLzSbUZFID+TiNA+BQc+eEwrMM38S1cAR/TJQqD7ZNY4DB4tSalGzUllcUQrWGKTzfnmY77xjK74ShVwU4+2HJ2Lz6fvrKf9MECh4IeabnC6qh/IqWxQuIhx6a1225gj3siZURgDvqt/6JqDg7vvUVoXSlrH/kP81T9Uq3TBAqfYNoMR/nsqP/rCyeEVUz7nd39tw/hdVVh8HFk/VZobv+Osxv2uxNP68BUw1+KsbHFwiIsGO5Q4JN92jW8PcPrd65Qh2g3HZvzKKuWYo8B2B4v6v0/7nU8JD6j6phvDH+qcjufhH84vtkOS2+4x5F+nrPOz7ztHOp4iObHlfCo/Ykva/q96jiN2xwD+vibGQ5FVmQZ9ExzLPYDQr3CG/anHdZM4TXHBs30+RU4jvH8D5ZB7TbnaQtVdXTRduRL1Yp+r3HHE3SYbDHofzoekv6AyjWhHXcxtf1cHYLASGtjl2pm8I7z29ByHZiGu7jKOW2k53cxKZ2i0RRaZXLKVPpfnu1Crmu0cFHNcl1kRvboHZuLtEBWaLbGlHjN21aVCCc1YB3yuNjoQw24BvJciG2XaDCwx4MbVR2zMapUF2k/T3V1k7gA67Med8AwSIsm+RECOVawzukOnNMcLyhGXdL/ntuRsT3eVAz6sLscYioBezygGXVIW9SxiZAwJxOA8QWNakaY9AhJPiATgL0d6YbSRe68HV1D4qIp6fLj92UCsJq7MSV+UX+4merecMT1DVIMGNYrllvLHgmq/lFSA/gStzegD/SBB3cz8NgVN2BtvIUYPAzCTa7PTsN6gqWGuXgSBXvNOAFzoaqSB1/O4OO1kOVzgPiZKY2aur2arFmp2lu/xrpXGza5rZDfJceYgbPopqLhlUGJvqUA+/nPHQDvT+xbtLYvsKADJ7umAG8NHnxfAS5z5LVFjQ1sSnCQyQzNsN3KvuBHaqeUScCgdLh4LAi8L/QgFGcU8AYF+Lyl0UJiUUaZ9GplUlQePFgcaufw70wCpvlqdisIfEFx7PUZBbxAWHt0PK8Ab8h84EOKSe/OKB9eoA6q7ILWZxkVpdcoUTrY5eJe4ToLnPLAG5UxRl23tC30oJFZA489ioaPBhv9nmLn5Rnlw+eUgLwrOKdQHTsQ5Zl5ekXpMmXysCmo9rFqo9kzmQLsbUd+JY23LJikzdUSAFMyBZgP0p6HBt9E4K21vMB7mQIMUzVV1q9lgxNKdu/zjAHepkSnKu+P6ptdqKYAvG0yAzj0jVFT4v9uS+JfarP5wEwAZt20jNbm2849QLP1ZZkADOPDZmPZT/BKqPLpWCYA40514gC9Qn/cJ9Qn94+nOzD8kC4pHnyy052hP7+uPWmbm/bAz2tEamIDelOlYffgKW2BsVBN7ljWa4RmtgxpSGamM7C3o/qIn0otC4VghvbgaWs6A8N8zaCt71uyx0J7LUNigLdNW+AmoYlJQxoSnor6mFJIWJ6uwOrjQVPiKfvThmqnVTzRND2BQ/OwhhSSh3sJC0u0E1enIzCf4FNfLS3HZjGZgiFJRptGpCKwuhbx1rQ/zMlXtJMLUg1YRAHmi7VVG5XelpFOn6i/8Rx5gSUe8oWtdJeY3SchwivnvCtVa+JZ4cjhTQlnI/Z3z/JxOLYxdz6et47Qnn3aq+E38x6MFtJH+JwvdkqRDaYJbXVbTAtM9IVWJPmQdMBlvclvagunY5NTd6rSVlyXJa+ERaxb/2/ri3eF5LEmnNX1S4YUEg+mvDnnW1bDfODkFbNiC/LGlMadr/cuWMqcFIdk/dGvL6fhi1MV17rekSQb5vQir1gXTfJpKRmsRun1CmLofW3NZIt16aO7tRAJxWV69SXna1Jve/Ix68AxtZDZc3p9B1PiVdclr/q1UFfj1hn2vJTx3TF6UTNTUqTlszHkhnpitbXAD6xNCdx5dgXjvIMaeFnv02hT0wiK8rKSCZv1LXUh1q2R/9h4SHIgBqzIeJE9mTTP7YPH7XD5zHgZz2C9Vw56Cy5MiilPJL+tdmfGU6bPWrVsSCHhiJtlHA0YUXWyK/ZoSkPC+HjLNQfL7cpVUDWswjsaSbezscJOt1jNRyTgdnlZeNoXpnqok/dCXAbP0VhiV9NFSLzG+iTOpLYL28SaT+IlmJQwzQ6E474wFXGhOKdZYiPka/rYJgT6K5jB4lrAPu8+PhlP2VfrMSU1vMphTMheu07hVgGwCtzMnsm+Ow56JViHl8NVZBISv+TDGilW9mwKS8NXyxLSlFgC62EwtHHlrS3hd3wlnjRk+ILaJHGz96eN2/l7sVhEqZmFV+gALMTh3q4xgXbAgTAL9tKXhox8ZTzDhnuSsfG/4BURQ/V/rKBSLIb1MA8m04t8IEdOnBjB0/QiTsI5sA4P0nm8bkb9GoCQdJPP8SRvy2nG51LAF7WmoRlSJ82QdGsXMX/0wJBCUg2sgYeTPmvJvpvPx7JEfMVD+5rH2zzLkzobnxgpzDQE1ZRYyqdn35WK2aUBsIEumXHBrvsmTzkUWMvKpdTWoSkfhh/SOZLuPkBk1vk5lsIOGOtt7UmPLfsu5oXp+B88Q7XRA5PyeanzuI8v4gO86flRsf73YA8YyhdiPu7F03jVlKb28TBT4g0swSLazhfDKOjFH/BkyvbrO1gLaMO7Yj9et2M/3sPbhrXK+47n6y1R2/8B63snlT2dN7MAAAAASUVORK5CYII=);
        }
        a {
            color: #404040;
        }
        a:hover {
            color: auto;
            text-decoration: none;
        }
        .a-pointer {
            cursor: pointer;
        }
        .top-right {
            z-index: 1;
            position: absolute;
            margin: 15px;
            right: 0;
        }
        #loading {
            top: 50%;
            left: 50%;
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
            position: absolute;
            z-index: 4;
        }
        #loading_bg {
            width: 100vw;
            height: 100vh;
            background-color: #ff000090;
            position: fixed;
            z-index: 5;
        }
    </style>
</head>
<body>
    <div id="loading_bg" style="display:none;"></div>
    <div id="loading" style="display:none;">
        <i class="fa fa-spin fa-circle-o-notch fa-5x"></i>
    </div>
    <div class="container">
        <a href="?p" class="top-right">
            <span><?= @$_SESSION['auth']; ?></span> <i class="fa fa-sign-in"></i>
        </a>
        <div class="col-md-12 text-center">
            <br class="visible-xs" />
            <h3 id="clock">&nbsp;</h3>
        </div>
        <div class="row">
            <?php if (!isset($_GET['auth'])): ?>
            <?php endif; ?>
            <div class="col-xs-12 col-md-4 col-md-offset-4 hidd">
                <div class="account-wall">
                    <?php if (isset($_GET['auth'])): ?>
                        <i class="profile-img alert-image"></i>
                    <?php else: ?>
                        <a href="./">
                            <img class="profile-img" src="../assets/img/logo-big.png" />
                        </a>
                        <i class="profile-img lock-image hidden"></i>
                        <h3 class="profile-title">Attendance Management System</h3>
                        <div class="a-pointer text-center">
                        <?php
                            $terminal_ok = false;
                            $terminal_cb = array();
                            $terminalloc = $loc = strtoupper($_COOKIE['terminalloc']);
                            foreach ($db->query("SELECT * FROM units ORDER BY location ASC;") as $check) {
                                $terminal_cb[strtoupper($check['locationid'])] = strtoupper($check['location']);
                                if (strtoupper($check['locationid']) == $terminalloc) {
                                    $terminal_ok = true;
                                    $terminalloc = '<h4>' . strtoupper($check['location']) . '</h4>';
                                    $terminalloc.= strtoupper($check['description']) ;
                                }
                            }
                            if (!$terminal_ok) {
                                $terminalloc = '<a href="?p=unit-edit&id=' . $terminalloc . '" title="Register Location" style="color:red">' . $terminalloc . '</a>';
                            }
                            echo $terminalloc;
                        ?>
                        </div>
                        <form id="changeloc" class="form-signin hidden" method="post">
                            <input name="terminalnum" type="hidden" value="<?= $_COOKIE['terminalnum']; ?>" />
                            <select name="terminalloc" class="form-control">
                            <?php foreach ($terminal_cb as $id => $cb): ?>
                                <option <?=  $loc==$id? 'selected' : '';?> value="<?= $id; ?>"><?= $cb; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </form>
                    <?php endif; ?>
                    <form class="form-signin" method="post" autocomplete="off" action="<?=isset($_GET['auth'])? base64_decode($_GET['auth']): @$_GET['url']; ?>">
                        <?php if (isset($_GET['auth'])): ?>
                        <input type="text" class="form-control" name="user" placeholder="Email" required style="text-align:center;" />
                        <input type="password" class="form-control" name="pass" placeholder="Password" required style="text-align:center;" />
                        <?php else: ?>
                        <div class="input-group" style="width:100%">
                            <input type="number" id="input" class="form-control" placeholder="NIP" required style="border-bottom-left-radius:0; border-bottom-color:transparent; text-align:center; width:80%" />
                            <input type="hidden" id="inputEl" name="user" />
                            <button type="button" class="input-group-addon-x btn btn-default" style="height:44px; width:20%;border-bottom-right-radius:0; border-bottom-left-radius:0; border-top-left-radius:0; border-bottom-color:transparent; border-left-color:transparent;" onclick="switcher(this)">ABC</button>
                        </div>
                        <?php endif; ?>
                        <input type="submit" class="btn btn-lg btn-default btn-block" value="<?= isset($_GET['auth'])? 'Login': 'Sign IN / OUT'; ?>" />
                    </form>
                </div>
                <div class="text-center" style="margin-top:10px;">
                    <a href="javascript:void()" onclick="if (confirm('Override All People That Forgot To Sign OUT?'))document.location='?p&allout';">
                        Sign OUT All People
                    </a>
                    &bullet;
                    <a href="javascript:void()" onclick="if (confirm('Change Unit Terminal?'))document.location='?p&unitout';">
                        Unit <?= (is_numeric($_COOKIE['terminalnum'])? 'No.' : '') . $_COOKIE['terminalnum']; ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <?php if ($db->single("SELECT COUNT(1) FROM timeclock WHERE whenout IS NULL") > 0): ?>
            <div class="panel panel-default" style="margin-top:20px;">
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th style='text-align:center; width:15%'>NIP</th>
                            <th style='text-align:left;   width:50%'>Name</th>
                            <th style='text-align:center; width:20%'>IN</th>
                            <th style='text-align:center; width:15%'>Lokasi</th>
                        </tr>
                    </thead>
                <?php
                    $data = array();
                    foreach ($db->query("SELECT * FROM timeclock WHERE locationin = '{$_COOKIE['terminalloc']}' AND whenout IS NULL ORDER BY locationin ASC, whenin DESC") as $aRow) {
                        $data[] = $aRow;
                    }
                    foreach ($db->query("SELECT * FROM timeclock WHERE locationin != '{$_COOKIE['terminalloc']}' AND whenout IS NULL ORDER BY locationin ASC, whenin DESC") as $aRow) {
                        $data[] = $aRow;
                    }
                    foreach ($data as $aRow) {
                        echo '<tr class="item" data-id="'.$aRow['userid'].'">';
                        echo '<td align="center">' . str_pad($aRow['userid'], 12, '0', STR_PAD_LEFT) . '</td>';
                        echo '<td>' . $aRow['username'] . '</td>';
                        echo '  <td align="center">';
                        echo '      <span>';
                        echo !empty($aRow['whenin'])? date('H:i', strtotime($aRow['whenin'])): '';
                        echo '      </span>';
                        echo (!empty($aRow['unitin'])? " <span style='color:silver; white-space:nowrap; display:initial !important; cursor:pointer;' title='Unit ".(is_numeric($aRow['unitin'])? 'No.' : '').$aRow['unitin']."'>[".$aRow['unitin']."]</span>": '');
                        echo '  </td>';
                        echo '<td align="center">' . $aRow['locationin'] . '</td>';
                        echo '</tr>';
                    }
                ?>
                </table>
            </div>
            <?php endif; ?>
        </div>
    </div>

    <script>
        document.getElementsByClassName('a-pointer')[0].addEventListener('click', function() {
            document.getElementsByClassName('a-pointer')[0].classList.add('hidden');
            document.getElementById('changeloc').classList.remove('hidden');
            document.getElementsByName('terminalloc')[0].focus();
        });
        document.getElementsByName('terminalloc')[0].addEventListener('blur', function() {
            document.getElementsByClassName('a-pointer')[0].classList.remove('hidden');
            document.getElementById('changeloc').classList.add('hidden');
        });
        document.getElementsByName('terminalloc')[0].addEventListener('change', function() {
            document.getElementById('changeloc').submit();
        });
        document.getElementsByTagName('form')[1].addEventListener('submit', function() {
            document.getElementById('loading').style['display'] = '';
            document.getElementById('loading_bg').style['display'] = '';

            var field = document.createElement('input');
            field.setAttribute('type', 'text');
            document.body.appendChild(field);

            setTimeout(function() {
                field.focus();
                setTimeout(function() {
                    field.setAttribute('style', 'display:none;');
                }, 50);
            }, 50);
        });

        var tap = new Event('TouchEvent');
        setTimeout(function() {
            document.getElementById('input').dispatchEvent(tap);
            document.getElementById('input').focus();
        }, 50);

        var switcher = function(that) {
            if (that.innerText != 'ABC') {
                that.innerText = 'ABC';
                document.getElementById('input').setAttribute('placeholder', 'NIP');
                document.getElementById('input').setAttribute('type', 'number');
            } else {
                that.innerText = '123';
                document.getElementById('input').setAttribute('placeholder', 'NAMA');
                document.getElementById('input').setAttribute('type', 'text');
            }
            document.getElementById('input').focus();
            document.getElementById('input').click();
        };
        setInterval(function() {
            var date = new Date(),
                hour = date.getHours(),
                minute = date.getMinutes(),
                second = date.getSeconds();
            var time = String("0" + hour).slice(-2) + ":" + String("0" + minute).slice(-2) + ":" + String("0" + second).slice(-2);
            document.getElementById('clock').innerHTML = time;
        }, 1000);
    </script>

    <script src="https://thelegion.co.id/assets/js/jquery.min.js"></script>
    <script src="assets/easy-autocomplete.min.js"></script>
    <script>
        $(function() {
            $("#input").easyAutocomplete({
                url: function(search) {
                    return "index.php?find=" + search;
                },
                getValue: "id",
                template: {
                    type: "description",
                    fields: {
                        description: "name"
                    }
                }
            });
            $('#input').on('change keyup', function() {
                $('#inputEl').val($(this).val());
            });
        });
    </script>
    <link href="assets/easy-autocomplete.min.css" rel="stylesheet" type="text/css" />
    <style>
        .easy-autocomplete {
            width: 80% !important;
        }
        .easy-autocomplete ~ button {
            width: 20% !important;
        }
        .easy-autocomplete > input {
            width: 100% !important;
        }
        .easy-autocomplete-container {
            top:  45px;
        }
        .easy-autocomplete-container li:only-child {
            min-height: 45px;
        }
    </style>
</body>
</html>
