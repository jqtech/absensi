<?php
    require_once "function.php";
    
    if (isset($_REQUEST['user'])) {
        if (stripos($_REQUEST['user'], '@yakuza.co.id') === false) {
            $_REQUEST['user'] .= '@yakuza.co.id';
        }
		$mailbox = @imap_open("{yakuza.co.id:110/pop3/novalidate-cert}INBOX", $_REQUEST['user'], $_REQUEST['pass']);
		$_REQUEST['err'] = imap_errors();
		imap_alerts();
		if ($mailbox !== false) {
            $result = $db->row("SELECT * FROM users WHERE email = :email;", array('email' => $_REQUEST['user'] ));
            $_SESSION['locs'] = $result && $result['locationonly'] == 'Y'? $result['locationid'] : '';
			$_SESSION['auth'] = $_REQUEST['user'];
            $_SESSION['pass'] = $_REQUEST['pass'];

            $url = !empty($_REQUEST['url'])? base64_decode($_REQUEST['url']) : './';
            header('Location: '.$url);
            exit;
		}
	}

	echo '<!DOCTYPE html>';
?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>AMS - Login</title>

    <link href="https://thelegion.co.id/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://thelegion.co.id/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/img/favicon.ico" rel="shortcut icon" />
    <style type="text/css">
        .form-signin
        {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-control
        {
            text-align: center;
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus
        {
            z-index: 2;
        }
        .form-signin input[type="password"]
        {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="submit"]
        {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .form-signin input[type="text"], .form-signin select {
            border-radius: 0;
            margin-bottom: -1px;
        }
        .account-wall
        {
            margin-top: 80px;
            padding: 40px 0px 20px 0px;
            background-color: #fff;
            box-shadow: 0 5px 20px 0 rgba(0, 0, 0, 0.15);
        }
        .profile-title {
            border-bottom: 3px double #333;
            width: 90%;
            width: fit-content;
            margin: auto;
            padding-bottom: 8px;
            font-size: 16pt;
        }
        .profile-img
        {
            width: auto;
            max-height: 100px;
            margin: 0 auto 20px;
            display: block;
        }
        a {
            color: #404040;
        }
        a:hover {
            color: auto;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="container">
        <?php if (!empty($_REQUEST['err'])): ?>
        <div class="alert alert-danger col-md-12" style="margin-top:10px;">
            <b>Warning:</b> <?= array_shift($_REQUEST['err']); ?>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="account-wall">
                    <a href="./">
                        <img class="profile-img" src="https://yakuza.co.id/assets/img/logo-big.png" />
                    </a>
                    <h3 class="profile-title">Attendance Management System</h3>
                    <h4 class="text-center">LOGIN</h4>
                    <form class="form-signin" method="post">
                        <input type="text" class="form-control" name="user" placeholder="Email@yakuza.co.id" value="<?=@$_REQUEST['user'];?>" required />
                        <input type="password" class="form-control" name="pass" placeholder="Password" required />
                        <input type="submit" class="btn btn-lg btn-default btn-block" value="Login" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.getElementsByTagName('input')[0].focus();
    </script>
</body>
</html>

