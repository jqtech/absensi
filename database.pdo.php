<?php

	class Database {
		private $pdo;
		private $sQuery;
		private $settings;
		private $bConnected = false;
		private $bLog;
		private $parameters;
		
		public function __sleep() {
			return array();
		}
		public function __construct($server = 'localhost', $user = 'root', $password = '', $database = '') { 			
			$this->settings = array(
				'host'=>$server,
				'user'=>$user,
				'password'=>$password,
				'dbname'=>$database
			);
			$this->bLog = array();
			$this->parameters = array();
			$this->Connect();
		}
		private function Connect() {
			$dsn = 'mysql:dbname='.$this->settings["dbname"].';host='.$this->settings["host"].'';
			try 
			{
				$this->pdo = new PDO($dsn, $this->settings["user"], $this->settings["password"]);
				$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$this->bConnected = true;
			}
			catch (PDOException $e) 
			{
				echo $this->ExceptionLog($e->getMessage());
				die();
			}
		}
		private function Init($query, $parameters = "") {
			if (!$this->bConnected) { $this->Connect(); }
			try {
				$this->sQuery = $this->pdo->prepare($query);
				$this->bindMore($parameters);
				if (!empty($this->parameters)) {
					foreach($this->parameters as $param) {
						$this->sQuery->bindValue($param["par"], $param["val"], $param['mix']);
					}		
				}
				$this->succes = $this->sQuery->execute();
				if (!$this->succes) {
					$this->ExceptionLog('Query failed', $this->sQuery->queryString);
				}
			}
			catch(PDOException $e)
			{
				echo $this->ExceptionLog($e->getMessage(), $query);
				die();
			}
			$par = $this->parameters;
			$this->parameters = array();
			return $par;
		}
		public function bind($para, $value, $type = PDO::PARAM_STR) {
			if (is_array($value)) {
				$type = $value[1];
				$value = $value[0];
			}
			$this->parameters[] = array("par" => ":" . $para, "val" => $value, "mix" => $type);
		}
		public function bindMore($parray) {
			if (empty($this->parameters) && is_array($parray)) {
				$columns = array_keys($parray);
				foreach($columns as $i => &$column)	{
					$this->bind($column, $parray[$column]);
				}
			}
		}
		public function query($query, $params = null, $fetchmode = PDO::FETCH_ASSOC) {
			$query = trim($query);
			$parameters = $this->Init($query,$params);
			if (stripos($query, 'select') === 0){
				$return = $this->sQuery->fetchAll($fetchmode);
			}
			elseif (stripos($query, 'insert') === 0) {
				$return = $this->pdo->lastInsertId();
			}
			elseif (stripos($query, 'update') === 0 || stripos($query, 'delete') === 0) {
				$return = $this->sQuery->rowCount();
			} else {
				$return = NULL;
			}
			return $return;
		}
		public function insert($table, $param) {
			foreach ($param as $name => $value) {
				$this->bind($name, $value);
			}
			$sql = 'insert into '.$table.' (`' .implode('`,`', array_keys($param)). '`)';
			$sql.= 'values ( :' .implode(', :',array_keys($param)). ')';
			return $this->query($sql);
		}
		public function update($table, $param, $filter) {
			$set_param = array();
			foreach ($param as $name => $value) {
				$set_param[] = "`{$name}`=:{$name}";
				$this->bind($name, $value);
			}
			$key_param = array();
			foreach ($filter as $name => $value) {
				$uniqueparam = $name.'_unique_'.count($key_param);
				$key_param[] = "`{$name}`=:{$uniqueparam}";
				$this->bind($uniqueparam, $value);
			}
			$sql = 'update '.$table.'  set ' .implode(', ', $set_param). ' where ' .implode(' and ', $key_param);
			return $this->query($sql);
		}
		public function delete($table, $param) {
			$set_param = array();
			foreach ($param as $name => $value) {
				$set_param[] = "`{$name}`=:{$name}";
				$this->bind($name, $value);
			}
			$sql = 'delete from '.$table.' where ' .implode(' and ', $set_param);
			return $this->query($sql);
		}
		public function column($query, $params = null) {
			$this->Init($query,$params);
			$Columns = $this->sQuery->fetchAll(PDO::FETCH_NUM);	
			$column = null;
			foreach($Columns as $cells) {
				$column[] = $cells[0];
			}
			return $column;
		}	
		public function row($query, $params = null, $fetchmode = PDO::FETCH_ASSOC) {				
			$this->Init($query,$params);
			return $this->sQuery->fetch($fetchmode);			
		}
		public function single($query,$params = null) {
			$this->Init($query,$params);
			return $this->sQuery->fetchColumn();
		}
		private function ExceptionLog($message, $sql = "") {
			if (!empty($sql)) {
				$message .= " \r\nRaw SQL : "  . $sql;
			}
			$this->bLog[] = $message;
			$log = implode(' .\n',$this->bLog);
			return $log;
		}	
	}
