<?php
    if (isset($_GET['allout'])) {
        $db->query("UPDATE timeclock SET whenout=NOW(), unitout=:unit, locationout=:loc1 WHERE locationin=:loc2 AND whenout IS NULL;", array(
            'loc1' => $_COOKIE['terminalloc'],
            'loc2' => $_COOKIE['terminalloc'],
            'unit' => $_COOKIE['terminalnum']
        ));
        header("Location: ./");
        exit;
    }

    if (isset($_GET['unitout'])) {
        setcookie('terminalloc', '',  time()-1, '/');
        setcookie('terminalnum', '',  time()-1, '/');
        header("Location: ./");
        exit;
    }

    if (isset($_GET['logout'])) {
        unset($_SESSION['auth']);
        header("Location: ./");
        exit;
    }

	if (empty($_SESSION['auth'])) {
		header('Location: auth.php?url='.base64_encode('//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));
		exit;
	}
