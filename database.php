<?php

// database login
$server = '127.0.0.1';
$user	= 'u1446529_itdept';
$pass 	= 'admin@ITd3pt';
$db 	= 'u1446529_yakuza';

if (!isset($noconnect)) {
	if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '192.168.1.110') {
		$db = new Database('localhost', 'root', '', 'yakuza');
	} else {
		$db = new Database($server, $user, $pass, $db);
	}

	session_start();
	date_default_timezone_set('Asia/Jakarta');
	
	$now = new DateTime();
	$min = $now->getOffset() / 60;
	$sgn = ($min < 0 ? -1 : 1);
	$min = abs($min);
	$hrs = floor($min / 60);
	$min -= $hrs * 60;

	$db->query("SET time_zone='".sprintf('%+d:%02d', $hrs * $sgn, $min)."';");
}
 
if (isset($_GET['setupdb'])) {
	$db->query("CREATE TABLE `users` (
	  `id` int NOT NULL PRIMARY KEY,
	  `name` varchar(255) NULL,
	  `email` varchar(255) NULL,
	  `email2` varchar(255) NULL,
	  `phone` varchar(255) NULL,
	  `phone2` varchar(255) NULL,
	  `address` varchar(255) NULL,
	  `company` varchar(255) NULL,
	  `department` varchar(255) NULL,
	  `position` varchar(255) NULL,
	  `locationid` varchar(255) NULL,
	  `locationonly` varchar(1) NOT NULL DEFAULT 'Y',
	  `status` varchar(255) NULL
	)");

	$db->query("CREATE TABLE `units` (
	  `locationid` varchar(255) NOT NULL PRIMARY KEY,
	  `location` varchar(255) NULL,
	  `description` varchar(255) NULL
	)");

	$db->query("CREATE TABLE `reasons` (
	  `reasonid` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  `description` varchar(255) NULL
	)");

	$db->query("CREATE TABLE `timeclock` (
	  `timeid` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  `userid` varchar(255) NULL,
	  `username` varchar(255) NULL,
	  `reason` varchar(255) NULL,
	  `locationin` varchar(255) NULL,
	  `whenin` datetime NULL,
	  `unitin` varchar(255) NULL,
	  `locationout` varchar(255) NULL,
	  `whenout` datetime NULL,
	  `unitout` varchar(255) NULL
	)");

	header("Location: ./");
	exit;
}
