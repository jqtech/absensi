<?php
	require_once("function.php");

    echo '<!DOCTYPE html>';
?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>AMS - Attendance Management System</title>

    <link href="https://thelegion.co.id/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://thelegion.co.id/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/img/favicon.ico" rel="shortcut icon" />
</head>
<body>
	<?php
		$id = intval(@$_GET['id']);
		$cls = 'success';
		$type = 'IN';

        if (isset($_GET['out'])) {
			$id = @$_GET['out'];
			$cls = 'warning';
			$type = 'OUT';
			$_POST['reason'] = @$_COOKIE['terminalnum'];

			$param = array(
                'id' => $id,
                'loc' => $_COOKIE['terminalloc'],
                'unit' => $_COOKIE['terminalnum']
            );
			$db->query("UPDATE timeclock SET locationout=:loc, unitout=:unit, whenout=NOW() WHERE timeid=:id;", $param);
		}
		else
		if (isset($_POST['reason'])) {
			$db->query("UPDATE timeclock SET reason=:reason WHERE timeid=:id;", array('id' => $id, 'reason' => $_POST['reason']));
		}

		$result = $db->row("SELECT * FROM timeclock WHERE timeid=:id;", array('id' => $id));
		if ($result) {
			$name = $result['username'] . ' [' . $result['userid'] . ']';
			if (!empty($result['reason'])) {
				$_POST['reason'] = $result['reason'];
			}
		} else {
			$name = '@Unknown User';
		}

		if (isset($_POST['reason'])) {
			if ($name == '@Unknown User') {
				$name = str_replace('@', '', $name);
				$msg = $name . ' &ndash; Failed To Sign ' . $type;
			} else {
				$msg = $name . ' &ndash; Sign ' . $type . ' Sucessfully';
			}
			echo '<meta HTTP-EQUIV="Refresh" Content="1; URL=./" />'; 
		}
	?>
	<div id="container">
		<div class="col-md-12" style="margin-top: 20px;">
		<?php
			if (!isset($_POST['reason'])) {
		?>
    		<div class="alert alert-default h3 text-center col-md-12" style="color:#333; background-color:#e6e6e6; border-color:#adadad;">
    			<?= str_replace('@', '', $name); ?> &ndash; Sign <?= $type; ?>:
    		</div>
    		<div class="row">
		<?php
				$col_md = 0;
				$result = $db->query("SELECT DISTINCT description FROM reasons ORDER BY description ASC;");
				foreach ($result as $row) {
					$col_md += 3;
		?>
				<form method="post" class="col-md-3" style="margin-bottom:20px;">
					<input type="submit" name="reason" class="btn btn-default" value="<?= $row['description']; ?>" style="height:50px; width:100%;" />
				</form>
		<?php
				}
				while ($col_md >= 12) {
					$col_md -= 12;
				}
				$col_md = 12 - $col_md;
		?>	
				<form method="post" class="col-md-<?= $col_md; ?>" style="margin-bottom:20px;">
					<div class="btn-group" style="width:100%">
						<input type="text" id="focus" name="reason" class="btn btn-default" placeholder="Other Reason" required style="height:50px; width:80%" />
						<button type="submit" class="btn btn-default" style="height:50px; width:20%">OK</button>
					</div>
				    <script>
				        document.getElementById('focus').focus();
				    </script>
				</form>
			</div>
		<?php
			} else {
		?>
			<div class="alert alert-<?= stripos($msg, 'sucessfully') !== false? $cls . ' h3' : 'danger'; ?> text-center col-md-12">
				<b class="<?= stripos($msg, 'sucessfully') !== false? 'hidden' : ''; ?>">ERROR:</b> <?= $msg; ?>
			</div>
		<?php
			}
		?>
		</div>
	</div>
</body>
</html>
